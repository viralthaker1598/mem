import { createSlice } from "@reduxjs/toolkit";
import {
  LOADING_STATE,
  QUANTITY_SELECTOR_STEP,
  VARIANT_SELECTOR_STEP,
} from "../../constants";
import { refineDesignAndCustomText } from "../../utils";

const initialState = {
  data: {
    data: LOADING_STATE,
  },
  useDesignElement: false,
  selectedVariant: {},
};

export const previewSlice = createSlice({
  name: "preview",
  initialState,
  reducers: {
    setPreviewData: (state, action) => {
      state.data = { ...(action.payload.res || {}) };
      /* === Check whether to use `customize_text` || `design_element` === */
      state.useDesignElement = action.payload.useDesignElement;
      state.productMetaData = action.payload.productMetaData || {};
      state.productData = action.payload.productData || {};
      /**
       * create variant selector option
       */
      const shouldVariantSelectorOptionRender =
        import.meta.env.MODE === "production"
          ? window.shouldVariantSelectorRender
          : true;
      const optionsList = action.payload?.productData?.product?.options;
      let variantOptions = [];
      if (optionsList?.length > 0 && shouldVariantSelectorOptionRender) {
        optionsList?.forEach((optionList, idx) => {
          if (!optionList.values || optionList.values?.length < 2) return;
          /* Insert based on the variant selection */
          variantOptions.push({
            isRequired: true,
            fieldlength: "",
            optionlist: optionList.values,
            optionKey: `option${idx + 1}`,
            otherOptionKeys: Array.from(Array(optionsList.length).keys())
              .map((i) => `option${i + 1}`)
              .filter((option, j) => idx !== j),
            currentOptionKey: `option${idx + 1}`,
            label: optionList.name,
            name: optionList.name,
            placeholder: "",
            fieldtype: VARIANT_SELECTOR_STEP,
          });
        });
      }

      const quantitySelector = {
        isRequired: true,
        fieldlength: "",
        label: "Quantity",
        name: "quantity",
        placeholder: "",
        fieldtype: QUANTITY_SELECTOR_STEP,
      };

      let inputArray = [
        ...variantOptions,
        ...refineDesignAndCustomText(
          action.payload?.res?.data?.customize_text || [],
          action.payload?.res?.data?.design_elements || []
        ),
      ];

      if (!window.adminPersonalization?.isAdminPanel) {
        state.inputArray = [...inputArray, quantitySelector];
      } else state.inputArray = inputArray;
      state.selectedVariant =
        action.payload?.productData?.product?.variants?.[0];
    },
    setVariant: (state, action) => {
      state.selectedVariant = action.payload;
    },
  },
});

/* === Selectors === */
export const getPreviewData = (state) => state.preview.data?.data || null;
export const getUseDesignElement = (state) => state.preview.useDesignElement;
export const getProductMetaData = (state) => state.preview.productMetaData;
export const getProductData = (state) =>
  state?.preview?.productData?.product || {};
export const getAllowedInputs = (state) => state?.preview?.inputArray || [];
export const getSelectedVariant = (state) =>
  state?.preview?.selectedVariant || [];

export const previewActions = previewSlice.actions;
window.personalizationPreviewActions = previewActions;
export default previewSlice.reducer;
