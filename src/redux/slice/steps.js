import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  total: 0,
  currentStep: 0,
  currentStepData: null,
  error: {},
  inputValues: window.adminPersonalization?.properties || {},
  loadingState: false,
  successState: false,
  errorState: false,
  errorMessage: "",
  selectedQuantity: 1,
};

export const stepsSlice = createSlice({
  name: "steps",
  initialState,
  reducers: {
    changeCurrentStep: (state, action) => {
      state.currentStep = action.payload.step;
      state.currentStepData = action.payload.stepData;
    },
    decrementCurrentStep: (state, action) => {
      state.currentStep--;
    },
    incrementCurrentStep: (state, action) => {
      state.currentStep++;
    },
    changeTotalSteps: (state, action) => {
      state.total = action.payload.step;
      state.currentStepData = action.payload.stepData;
    },
    changeCurrentData: (state, action) => {
      state.currentStepData = action.payload;
    },
    setErrorData: (state, action) => {
      state.error = action.payload;
    },
    setInputValues: (state, action) => {
      state.inputValues = action.payload;
    },
    setLoadingState: (state, action) => {
      state.loadingState = action.payload;
    },
    setSuccessState: (state, action) => {
      state.successState = action.payload;
    },
    setErrorState: (state, action) => {
      state.errorState = action.payload.error;
      state.errorMessage = action.payload.errorMessage;
    },
    changeSelectedQuantity: (state, action) => {
      state.selectedQuantity = action.payload;
    },
    resetStepsState: () => initialState,
  },
});

/* === Selectors === */
export const getSteps = (state) => state?.steps || {};
export const getStepCurrentStep = (state) => state?.steps?.currentStep || 0;
export const getStepCurrentStepData = (state) =>
  state?.steps?.currentStepData || {};
export const getStepTotalSteps = (state) => state?.steps?.total || 0;
export const getCustomizationData = (state) => state?.preview?.data?.data || {};
export const getDesignElementsData = (state) =>
  state?.preview?.data?.design_elements || {};
export const getCustomTextData = (state) =>
  state?.preview?.data?.customize_text || {};
export const getErrorData = (state) => state?.steps?.error || {};
export const getInputValues = (state) => state?.steps?.inputValues || {};
export const getLoadingState = (state) => state?.steps?.loadingState || false;
export const getSuccessState = (state) => state?.steps?.successState || false;
export const getErrorState = (state) => state?.steps?.errorState || false;
export const getErrorStateMessage = (state) => state?.steps?.errorMessage || "";
export const getSelectedQuantity = (state) =>
  state?.steps?.selectedQuantity || 1;

export const stepsActions = stepsSlice.actions;
export default stepsSlice.reducer;
