import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	open: false,
};

export const modalSlice = createSlice({
	name: "modal",
	initialState,
	reducers: {
		toggle: (state, action) => {
			state.open = action.payload || !state.modal.open;
		},
		open: (state) => {
			state.open = true;
		},
		close: (state) => {
			state.open = false;
		},
	},
});

/* Selectors */
export const getModalState = (state) => state.modal;

export const modalActions = modalSlice.actions;
export default modalSlice.reducer;
