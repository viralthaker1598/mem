import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	cart: {},
};

export const cartSlice = createSlice({
	name: "steps",
	initialState,
	reducers: {
		setCart: (state, action) => {
			state.cart = action.payload;
		},
	},
});

/* === Selectors === */
export const getCart = (state) => state?.cart || {};

export const stepsActions = cartSlice.actions;
export default cartSlice.reducer;
