import { call, delay, put, select, takeLatest } from "redux-saga/effects";
import { SHOP_NAME } from "../../constants";
import { modalActions } from "../slice/modal";
import {
  getInputValues,
  getSelectedQuantity,
  stepsActions,
} from "../slice/steps";
import { ADD_TO_CART, FETCH_CART, UPDATE_CART } from "../types/cartType";

function* fetchCart(action) {
  try {
  } catch (err) {
    console.log(err);
  }
}

function* updateCart(action) {
  try {
  } catch (err) {
    console.log(err);
  }
}

const uploadSVG = async (body) => {
  const response = await fetch(
    "https://hyntl2m33i.execute-api.us-east-2.amazonaws.com/previewImage?action=upload-image",
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        shopname:
          import.meta.env.mode === "production"
            ? window.shopUrl.replace("https://", "")
            : SHOP_NAME,
      },
      body: JSON.stringify(body),
    }
  );
  return await response.json();
};

function* successAPIFun(error = false, errorMessage) {
  yield put(stepsActions.setLoadingState(false));
  error
    ? yield put(stepsActions.setErrorState({ error: true, errorMessage }))
    : yield put(stepsActions.setSuccessState(true));
  if (error) return;
  yield delay(error ? 5000 : 2500);
  yield put(modalActions.close());
  yield put(stepsActions.changeCurrentStep({ step: 0, stepData: {} }));
  yield put(stepsActions.changeTotalSteps({ total: 0, stepData: {} }));
  yield put(stepsActions.setSuccessState(false));
  yield put(stepsActions.setErrorState({ error: false, errorMessage: "" }));
  yield put(stepsActions.setInputValues({}));
  yield put(stepsActions.changeSelectedQuantity(1));
}

function* addToCart(action) {
  const { preview_image, final_image, variantId } = action.payload;
  const quantity = yield select(getSelectedQuantity);

  try {
    /* Upload SVGs to S3 */
    const response = yield call(uploadSVG, {
      base64PreviewImage: preview_image,
      base64FinalImage: final_image,
      filename: null,
    });
    const { previewFileUrl, productionFileUrl } = response.data || {};
    const inputValues = yield select(getInputValues);

    /* Add to cart API */
    const res = yield call(() =>
      fetch("/cart/add.js", {
        method: "post",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          items: [
            {
              quantity,
              id: variantId,
              properties: {
                productionFile: productionFileUrl,
                previewFile: previewFileUrl,
                ...inputValues,
              },
            },
          ],
        }),
      })
    );
    const shopifyRes = yield call(() => res.json());
    if (shopifyRes.items) {
      yield successAPIFun();
    } else {
      yield successAPIFun(true, shopifyRes?.description);
    }
  } catch (err) {
    yield successAPIFun(true, err.description);
    console.log(err);
  }
}

export function* cartSaga() {
  yield takeLatest(FETCH_CART, fetchCart);
  yield takeLatest(UPDATE_CART, updateCart);
  yield takeLatest(ADD_TO_CART, addToCart);
}
