import { all } from "redux-saga/effects";
import { cartSaga } from "./cartSaga";
import { previewSaga } from "./previewSaga";

export function* rootSaga() {
	yield all([previewSaga(), cartSaga()]);
}
