import { call, delay, put, takeLatest } from "redux-saga/effects";
import {
  buildPreviewUrl,
  POSITIVE_BOOLEAN_STRING,
  previewReqOptionsHandler,
  STATIC_DATA,
} from "../../constants";
import { previewActions } from "../slice/preview";
import { FETCH_PRODUCT_METADATA } from "../types/previewType";

const fetchData = async (previewURL) => {
  const previewReqOptions = previewReqOptionsHandler(window.shopUrl);
  let response = await fetch(previewURL, previewReqOptions);
  return await response.json();
};

const fetchProductData = async () => {
  if (window?.adminPersonalization?.isAdminPanel) return window.productMetaData;

  let response = await fetch(window.productMetaData.product.handle + ".json");
  return await response.json();
};

function* fetchProductMetadata() {
  try {
    const data = STATIC_DATA;
    /* GET INIT CALL */
    const productId =
      import.meta.env.MODE === "production"
        ? window?.productMetaData?.product.id
        : data.product.id;
    const previewURL = buildPreviewUrl(productId);
    const res = yield call(fetchData, previewURL);
    if (!res?.data) {
      /* Neither customize_text nor design_element is present */
    }
    let productData = undefined;
    productData =
      import.meta.env.MODE === "production"
        ? yield call(fetchProductData)
        : data;
    let productMetaData;
    /* === check if need to use design_elements or customize_text
		- if customized text then fetch variant based SVGs
		*/
    let useDesignElement =
      (
        res.data?.design_elements?.filter(
          (designElement) =>
            designElement?.attributes?.is_enable === POSITIVE_BOOLEAN_STRING &&
            Boolean(
              designElement?.personalization_options &&
                designElement?.personalization_options?.length > 0
            )
        ) || []
      ).length > 0;

    /* === fetch product monogram metadata === */
    const customizedPreviewURL = buildPreviewUrl(productId, false);
    const customizedRes = yield call(fetchData, customizedPreviewURL);
    productMetaData = customizedRes.data;

    yield put(
      previewActions.setPreviewData({
        res,
        productMetaData,
        useDesignElement,
        productData: productData,
      })
    );
    yield delay(100);
    const contentLoadedEvent = new CustomEvent("contentLoaded", {
      detail: { isContentLoaded: true },
    });
    dispatchEvent(contentLoadedEvent);
  } catch (err) {
    console.log(err);
  }
}

export function* previewSaga() {
  yield takeLatest(FETCH_PRODUCT_METADATA, fetchProductMetadata);
}
