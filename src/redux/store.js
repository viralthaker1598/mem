import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";
import { rootSaga } from "./saga";
import { modalSlice } from "./slice/modal";
import { previewSlice } from "./slice/preview";
import { stepsSlice } from "./slice/steps";

const sagaMiddleware = createSagaMiddleware({});
export const store = configureStore({
  reducer: {
    preview: previewSlice.reducer,
    modal: modalSlice.reducer,
    steps: stepsSlice.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat([sagaMiddleware]),
});
sagaMiddleware.run(rootSaga);

window.store = window.store || store;
