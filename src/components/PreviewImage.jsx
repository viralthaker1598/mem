export const PreviewImage = () => {
  return (
    <div
      className="product__media-item--previewElement is-active"
      style={{ position: "relative" }}
    >
      <noscript>
        <div className="product__media media gradient global-media-settings">
          <img src={""} width="100%" height="100%" alt={""} />
        </div>
      </noscript>
      <div
        className="product__media media media--transparent gradient global-media-settings"
        id="image-container"
      >
        <img src={""} width="100%" height="100%" alt={""} />
      </div>
    </div>
  );
};
