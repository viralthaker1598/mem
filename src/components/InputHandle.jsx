import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getAllowedInputs,
  getProductData,
  getUseDesignElement,
} from "../redux/slice/preview";
import {
  getErrorData,
  getInputValues,
  getSelectedQuantity,
  getStepCurrentStepData,
  stepsActions,
} from "../redux/slice/steps";
import { previewActions } from "../redux/slice/preview";
import "./../styles/inputHandle.scss";
import { PreviewFooter } from "./PreviewFooter";
import { MenuItem, Select, TextField, Tooltip } from "@mui/material";
import {
  POSITIVE_BOOLEAN_STRING,
  QUANTITY_SELECTOR_STEP,
  VARIANT_SELECTOR_STEP,
} from "../constants";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import { ReactComponent as WarningIcon } from "../assets/warning.svg";
import { ReactComponent as SuccessIcon } from "../assets/successIcon.svg";

function InputHandle() {
  const dispatch = useDispatch();
  const productData = useSelector(getProductData);
  const [selectedVariant, setSelectedVariant] = useState(
    window?.adminPersonalization?.isAdminPanel
      ? window.adminPersonalization.selectedVariant
      : productData.variants[0]
  );

  const liquidProductData = window.productMetaData?.product;
  const liquidSelectedVariant = liquidProductData?.variants?.find(
    (variant) => variant.id === selectedVariant.id
  );
  const currentStepData = useSelector(getStepCurrentStepData);
  const stepDataLocal = useSelector(getAllowedInputs);
  const filteredStepper = stepDataLocal.filter(
    (label) => Object.keys(label).length > 1
  );
  const errorData = useSelector(getErrorData);
  const inputValues = useSelector(getInputValues);
  const useDesignElement = useSelector(getUseDesignElement);
  const itemQuantity = useSelector(getSelectedQuantity);
  const [activeImgIdx, setActiveImgIdx] = useState(0);

  const isDisplay = (currentInputData) => {
    let show = false;
    if ("personalization_options" in currentStepData) {
      show = currentInputData.name === currentStepData.name;
    } else {
      show = currentInputData.label === currentStepData.label;
    }
    return show;
  };

  useEffect(() => {
    /* ==== CHANGE ALL VARIANTS INPUT AS PER SELECTION ==== */
    document.querySelectorAll("[name^=id]").forEach((ele) => {
      ele.value = selectedVariant?.id;
    });
    dispatch(previewActions.setVariant(selectedVariant));
  }, [selectedVariant]);

  const renderInput = (currentInputData, index) => {
    const type = currentInputData?.fieldtype
      ? currentInputData?.fieldtype
      : currentInputData?.design_file
      ? "design_input"
      : `design_${currentInputData.type}`;
    const requiredKey = useDesignElement ? "is_required" : "isRequired";

    switch (type) {
      case "input": {
        return (
          <div className="mono--app--field input-full-width" key={index}>
            <TextField
              variant="outlined"
              name={`properties[${currentInputData.label}]`}
              id={`monofield_${index}`}
              required={
                currentInputData?.[requiredKey] === POSITIVE_BOOLEAN_STRING
              }
              className="required"
              placeholder={
                currentInputData.placeholder +
                (currentInputData?.[requiredKey] === POSITIVE_BOOLEAN_STRING
                  ? "*"
                  : "")
              }
              maxLength={currentInputData.fieldlength}
              value={
                inputValues[currentInputData.label || currentInputData.name]
              }
              onChange={(e) => {
                e.preventDefault();
                if (e.target.value.length > currentInputData?.fieldlength) {
                  e.stopPropagation();
                  return;
                }
                let newErrorObj = { ...errorData };

                let isRegexInValid = false;

                try {
                  let regexPattern = useDesignElement
                    ? currentInputData?.pattern
                    : currentInputData?.fieldpattern;
                  regexPattern = regexPattern.replace("?-", "?");
                  regexPattern = regexPattern.replace("/]", "]");
                  isRegexInValid =
                    !new RegExp(regexPattern).test(e.target.value) ||
                    ((useDesignElement
                      ? currentInputData?.is_required
                      : currentInputData?.isRequired) &&
                      e.target.value === 0);
                } catch (e) {
                  isRegexInValid = true;
                }

                if (isRegexInValid) {
                  e.stopPropagation();
                  newErrorObj[currentInputData.label] = true;
                } else delete newErrorObj[currentInputData.label];
                dispatch(stepsActions.setErrorData(newErrorObj));
                dispatch(
                  stepsActions.setInputValues({
                    ...(inputValues || {}),
                    [currentInputData.label || currentInputData.name]:
                      e.target.value?.toUpperCase(),
                  })
                );
              }}
              error={Object.keys(errorData).includes(currentInputData.label)}
              helperText={
                Object.keys(errorData).includes(currentInputData.label) &&
                (currentInputData?.patternMessage ||
                  currentInputData?.pattern_message ||
                  "Please enter valid Input")
              }
              fullWidth={true}
            />
            {Object.keys(errorData).includes(currentInputData.label) ? (
              <WarningIcon />
            ) : inputValues[currentInputData.label || currentInputData.name]
                ?.length > 0 ? (
              <SuccessIcon />
            ) : null}
          </div>
        );
      }
      case "dropdownlist":
      case "dropdown": {
        const optionsString =
          currentInputData?.["optionlist"] || currentInputData?.["value"] || "";
        const value =
          inputValues[currentInputData.label || currentInputData.name];
        return (
          <div className="mono--app--field drop-down-full-width" key={index}>
            <select
              name={`properties[${currentInputData.label}]`}
              id={`monofield_${index}`}
              placeholder={currentInputData.placeholder}
              onChange={(e) => {
                dispatch(
                  stepsActions.setInputValues({
                    ...(inputValues || {}),
                    [currentInputData.label || currentInputData.name]:
                      e.target.value,
                  })
                );
              }}
              style={
                !value || value === currentInputData.placeholder
                  ? {
                      color: "#a2a2a2",
                      height: "40px",
                    }
                  : {
                      height: "40px",
                    }
              }
              defaultValue={currentInputData.placeholder}
              required={
                currentInputData?.[requiredKey] === POSITIVE_BOOLEAN_STRING
              }
              value={value}
            >
              {optionsString?.split(",")?.map((item, index) => {
                return (
                  <option value={item} key={index} defaultValue={index === 0}>
                    {item}
                  </option>
                );
              })}
            </select>
            <div className="notify-icon">
              {Object.keys(errorData).includes(currentInputData.label) ? (
                <WarningIcon />
              ) : inputValues[currentInputData.label || currentInputData.name]
                  ?.length > 0 ? (
                <SuccessIcon />
              ) : null}
            </div>
          </div>
        );
      }
      case "design_input": {
        return (
          <div className="personalization-options" key={index}>
            {currentInputData?.personalization_options?.map((item, idx) => {
              return (
                <React.Fragment key={idx}>
                  {renderInput(
                    { ...item.attributes, fieldtype: item.type },
                    `${index}${idx}`
                  )}
                </React.Fragment>
              );
            })}
          </div>
        );
      }
      case VARIANT_SELECTOR_STEP: {
        const optionsList =
          currentInputData?.["optionlist"] || currentInputData?.["value"] || "";
        const isImageBasedVariant = ["color"].includes(currentInputData?.name);
        const currentOptionKey = currentInputData["currentOptionKey"];

        return (
          <div className="mono--app--field drop-down-full-width" key={index}>
            {isImageBasedVariant ? (
              productData?.images?.map((imageObj, idx) => {
                /* Find A Variant Object specific to option */
                const variantObj = productData?.variants?.find(
                  (variant) => variant.id === imageObj?.variant_ids?.[0]
                );
                const currentOptionValue =
                  variantObj?.[currentOptionKey]; /* Color */

                /* Check if Variant color is available or not */
                const currentCompatibleVariantArr =
                  liquidProductData?.variants?.filter(
                    (variant) =>
                      variant?.[currentOptionKey] === currentOptionValue &&
                      variant?.available
                  );
                const isVariantAvailable =
                  currentCompatibleVariantArr.length > 0;

                /* Switch to available Variant for activeImgIdx */
                if (activeImgIdx === idx && !isVariantAvailable) {
                  const nextAvailableVariant =
                    liquidProductData?.variants?.find(
                      (variant) => variant?.available
                    );

                  if (nextAvailableVariant) {
                    const newIdx = productData?.images?.findIndex((imgObj) =>
                      imgObj.variant_ids.includes(nextAvailableVariant?.id)
                    );
                    setActiveImgIdx(newIdx);
                    /* ==== CHANGE ALL VARIANTS INPUT AS PER SELECTION ==== */
                    document.querySelectorAll("[name^=id]").forEach((ele) => {
                      ele.value = nextAvailableVariant.id;
                    });
                    dispatch(previewActions.setVariant(nextAvailableVariant));
                    setSelectedVariant(nextAvailableVariant);
                  }
                } else if (
                  activeImgIdx === idx &&
                  isVariantAvailable &&
                  !liquidSelectedVariant?.available &&
                  currentCompatibleVariantArr.length > 0 &&
                  isImageBasedVariant &&
                  ["color"].includes(currentStepData.name)
                ) {
                  /* === Change Variant === */
                  setTimeout(() => {
                    document.querySelectorAll("[name^=id]").forEach((ele) => {
                      ele.value = currentCompatibleVariantArr[0].id;
                    });
                    dispatch(
                      previewActions.setVariant(currentCompatibleVariantArr[0])
                    );
                    setSelectedVariant(currentCompatibleVariantArr[0]);
                  }, 10);
                }

                return (
                  <Tooltip
                    title={currentOptionValue || ""}
                    value={currentOptionValue || ""}
                  >
                    <div
                      style={
                        !isVariantAvailable
                          ? {
                              cursor: "not-allowed",
                              opacity: "0.5",
                            }
                          : { cursor: "pointer" }
                      }
                    >
                      <img
                        key={idx}
                        className={idx === activeImgIdx ? "active-image" : ""}
                        onClick={() => {
                          if (!isVariantAvailable) return;
                          setActiveImgIdx(idx);
                          if (variantObj) {
                            /* ==== CHANGE ALL VARIANTS INPUT AS PER SELECTION ==== */
                            document
                              .querySelectorAll("[name^=id]")
                              .forEach((ele) => {
                                ele.value = variantObj.id;
                              });
                            dispatch(previewActions.setVariant(variantObj));
                            setSelectedVariant(variantObj);
                          }
                        }}
                        src={imageObj?.src}
                      />
                    </div>
                  </Tooltip>
                );
              })
            ) : (
              <Select
                fullWidth={true}
                onChange={(e) => {
                  const variant = productData?.variants?.find((variantObj) => {
                    let isFound = false;
                    if (
                      variantObj[currentInputData.optionKey] === e.target.value
                    ) {
                      let boolArr = [];
                      currentInputData.otherOptionKeys.forEach(
                        (optionKey, idx) => {
                          boolArr[idx] =
                            selectedVariant[optionKey] ===
                            variantObj[optionKey];
                        }
                      );
                      isFound =
                        boolArr.length ===
                        boolArr.filter((bool) => bool).length;
                    }
                    return isFound;
                  });

                  if (variant) {
                    /* ==== CHANGE ALL VARIANTS INPUT AS PER SELECTION ==== */
                    document.querySelectorAll("[name^=id]").forEach((ele) => {
                      ele.value = variant.id;
                    });
                    dispatch(previewActions.setVariant(variant));
                    setSelectedVariant(variant);
                  }
                }}
                style={{
                  height: "60px",
                }}
                defaultValue={selectedVariant[currentOptionKey]}
                value={selectedVariant[currentOptionKey]}
              >
                {optionsList?.map((value, j) => (
                  <MenuItem key={`${value}_${j}`} value={value}>
                    {value}
                  </MenuItem>
                ))}
              </Select>
            )}
          </div>
        );
      }

      case QUANTITY_SELECTOR_STEP:
        return (
          <div className="mono--app--field quantity-wrapper" key={index}>
            <div className="pointer back-arrow-container">
              <ArrowBackIosIcon
                onClick={() => {
                  if (itemQuantity > 1)
                    dispatch(
                      stepsActions.changeSelectedQuantity(itemQuantity - 1)
                    );

                  dispatch(
                    stepsActions.setErrorState({
                      error: false,
                      errorMessage: "",
                    })
                  );
                }}
              />
            </div>
            <div className="quantity-container">
              <div className="quantity-display">{itemQuantity}</div>
            </div>
            <div className="pointer forward-arrow-container">
              <ArrowForwardIosIcon
                onClick={() =>
                  dispatch(
                    stepsActions.changeSelectedQuantity(itemQuantity + 1)
                  )
                }
              />
            </div>
          </div>
        );

      default:
        return null;
    }
  };

  return (
    <div className="mema--mono--app input-handle-wrapper">
      <form>
        {filteredStepper.map((currentInputData, index) => {
          return (
            <div
              key={currentInputData?.label}
              style={{
                display: isDisplay(currentInputData) ? "initial" : "none",
              }}
            >
              {currentInputData
                ? renderInput(currentInputData, index + 1)
                : null}
            </div>
          );
        })}
        <PreviewFooter />
      </form>
    </div>
  );
}

export default InputHandle;
