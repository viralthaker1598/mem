import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import {
  getProductData,
  getProductMetaData,
  getSelectedVariant,
  getUseDesignElement,
} from "../redux/slice/preview";
import { getStepCurrentStepData } from "../redux/slice/steps";
import InputHandle from "./InputHandle";
import { PreviewImage } from "./PreviewImage";
import Stepper from "./Stepper";

const Preview = () => {
  const previewState = useSelector(getProductMetaData);
  const productData = useSelector(getProductData);
  const currentState = useSelector(getStepCurrentStepData);
  const selectedVariant = useSelector(getSelectedVariant);
  const useDesignElement = useSelector(getUseDesignElement);

  /**
   * how to make this dynamic enough,
   * this generally comes from currentStepData, but currently at step 1, we dont image object as we have variant object.
   * */
  const designElementObject = useSelector(
    (state) => state?.preview?.data?.data?.design_elements?.[0]
  );

  useEffect(() => {
    window.monoGramVariants = productData.variants || [];
  }, [productData]);

  useEffect(() => {
    if (selectedVariant) {
      window.monogramSelectedVariant = () => {
        return selectedVariant;
      };
    }
  }, [selectedVariant]);

  useEffect(() => {
    if (
      !previewState ||
      Object.keys(previewState).length === 0 ||
      !currentState ||
      Object.keys(currentState).length === 0
    )
      return;
    /**
     * Script already exists then do not create new scripts.
     */
    const exist = document.querySelector("[data-id='#react-script']");

    /**
     * Background is not working
     * Window elements are used by script.
     */
    window.monoGramData = {
      ...previewState,
      backgroundImageSrc: useDesignElement
        ? designElementObject?.attributes?.background_image
        : null,
      /**
       * This is dynamic url to fetch image.
       */
      designElementSrc: useDesignElement ? designElementObject?.template : null,
    };
    const scriptTag = document.createElement("script");
    scriptTag.setAttribute("data-id", "#react-script");

    /* Get script data from API */
    scriptTag.innerHTML = previewState.script;

    if (exist) {
      try {
        /**
         * Reappending new script and removing event listener for every variant select.
         */
        document?.body?.removeChild(exist);
        window.removeEventListener("input", window.initFunction);
      } catch (err) {}
    } else {
    }
    document.querySelector("body").appendChild(scriptTag);
  }, [currentState, selectedVariant, previewState]);

  return (
    <>
      <PreviewImage />
      <Stepper />
      <InputHandle />
    </>
  );
};

export default Preview;
