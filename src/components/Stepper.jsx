import React, { useEffect } from "react";
import { Step, StepLabel, Stepper as MuiStepper } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import {
  getErrorData,
  getInputValues,
  getStepCurrentStep,
  getStepCurrentStepData,
  getSteps,
  stepsActions,
} from "../redux/slice/steps";
import {
  getAllowedInputs,
  getSelectedVariant,
  getUseDesignElement,
} from "../redux/slice/preview";
import "./../styles/stepper.scss";
import {
  PERSONALIZATION_TEXT,
  POSITIVE_BOOLEAN_STRING,
  QUANTITY_SELECTOR_STEP,
  VARIANT_SELECTOR_STEP,
} from "../constants";
import { capitalize } from "lodash";

function Stepper() {
  const dispatch = useDispatch();
  const stepDataLocal = useSelector(getAllowedInputs);
  const filteredStepper = stepDataLocal.filter(
    (label) => Object.keys(label).length > 1
  );
  const steps = useSelector(getSteps);
  const selectedVariant = useSelector(getSelectedVariant);
  const currentStep = useSelector(getStepCurrentStep);
  const currentStepData = useSelector(getStepCurrentStepData);
  const useDesignElement = useSelector(getUseDesignElement);
  const inputValues = useSelector(getInputValues);
  const errorData = useSelector(getErrorData);

  useEffect(() => {
    let step = filteredStepper.length;
    /* For Admin Step */
    dispatch(
      stepsActions.changeTotalSteps({
        step,
        stepData: filteredStepper?.[0] || {},
      })
    );
  }, [stepDataLocal]);

  const onStepClick = (step) => {
    dispatch(
      stepsActions.setErrorState({
        error: false,
        errorMessage: "",
      })
    );
    if (step - currentStep > 1) return false;
    if (
      currentStep > step ||
      currentStepData?.fieldtype === VARIANT_SELECTOR_STEP
    ) {
      dispatch(
        stepsActions.changeCurrentStep({
          step,
          stepData: filteredStepper[step],
        })
      );
    } else {
      if (useDesignElement) {
        if (currentStepData?.fieldtype === VARIANT_SELECTOR_STEP) return false;

        const requiredSubFieldForDesignElements =
          currentStepData?.personalization_options?.filter((item, index) => {
            return (
              inputValues[`${currentStep + 1}${index}`]?.length > item.max_count
            );
          }) || [];

        /* Disable when inputs length are greater than required */
        if (requiredSubFieldForDesignElements?.length > 0) return false;

        if (
          Object.keys(errorData)?.length > 0 ||
          currentStepData?.personalization_options?.filter(
            (item, index) =>
              item?.attributes?.is_required === POSITIVE_BOOLEAN_STRING &&
              (!inputValues?.[`${currentStep + 1}${index}`] ||
                inputValues?.[`${currentStep + 1}${index}`]?.length === 0) &&
              ["input"].includes(item?.type)
          )?.length > 0
        )
          return false;
      } else {
        /* Customized Text validations */
        if (
          Object.keys(errorData)?.length > 0 ||
          (currentStepData?.isRequired &&
            (!inputValues?.[currentStep + 1] ||
              inputValues?.[currentStep + 1]?.length === 0) &&
            ["input"].includes(currentStepData?.fieldtype))
        )
          return false;
      }

      dispatch(
        stepsActions.changeCurrentStep({
          step,
          stepData: filteredStepper[step],
        })
      );
    }
  };

  const shouldStepperRender = filteredStepper.length > 1;

  return (
    <div className="step-wrapper">
      <div className="step-wrapper-btn">
        {shouldStepperRender ? (
          <MuiStepper activeStep={steps.currentStep} nonLinear>
            {filteredStepper.map((label, index) => {
              return (
                <Step
                  className={
                    steps.currentStep > index
                      ? "step-pointer completed"
                      : steps.currentStep === index
                      ? "step-pointer current"
                      : "step-pointer next"
                  }
                  key={index}
                  onClick={() => onStepClick(index)}
                >
                  <StepLabel />
                </Step>
              );
            })}
          </MuiStepper>
        ) : null}
      </div>
      {(steps?.currentStepData?.label || steps?.currentStepData?.name) && (
        <>
          <div className={"step-wrapper-label"}>
            {capitalize(
              steps?.currentStepData?.label || steps?.currentStepData?.name
            )}
          </div>
          {steps?.currentStepData?.fieldtype !== QUANTITY_SELECTOR_STEP ? (
            `design_${steps?.currentStepData?.type}` === "design_input" ? (
              <span>{PERSONALIZATION_TEXT}</span>
            ) : (
              <span>{`Which ${
                steps?.currentStepData?.label || steps?.currentStepData?.name
              } would you like?`}</span>
            )
          ) : (
            <span>How many items you want ?</span>
          )}
        </>
      )}
      {/* this is variant id which needs to be dynamic */}
      <input type="hidden" name="id" value={selectedVariant.id} />
    </div>
  );
}

export default Stepper;
