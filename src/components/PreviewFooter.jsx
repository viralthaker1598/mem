import { Button } from "@mui/material";
import { Buffer } from "buffer";
import { useDispatch, useSelector } from "react-redux";
import { POSITIVE_BOOLEAN_STRING, VARIANT_SELECTOR_STEP } from "../constants";
import {
  getAllowedInputs,
  getSelectedVariant,
  getUseDesignElement,
} from "../redux/slice/preview";
import CircularProgress from "@mui/material/CircularProgress";
import {
  getErrorData,
  getErrorState,
  getErrorStateMessage,
  getInputValues,
  getLoadingState,
  getStepCurrentStep,
  getStepCurrentStepData,
  getStepTotalSteps,
  getSuccessState,
  stepsActions,
} from "../redux/slice/steps";
import { ADD_TO_CART } from "../redux/types/cartType";
import { svgToBase64 } from "../utils";
import "./../styles/footer.scss";
import DoneIcon from "@mui/icons-material/Done";

export const PreviewFooter = () => {
  const dispatch = useDispatch();
  const currentStep = useSelector(getStepCurrentStep);
  const selectedVariant = useSelector(getSelectedVariant);
  const totalStep = useSelector(getStepTotalSteps);
  const stepDataLocal = useSelector(getAllowedInputs);
  const filteredStepper = stepDataLocal.filter(
    (label) => Object.keys(label).length > 1
  );
  const currentStepData = useSelector(getStepCurrentStepData);
  const errorData = useSelector(getErrorData);
  const inputValues = useSelector(getInputValues);
  const useDesignElement = useSelector(getUseDesignElement);
  const loadingState = useSelector(getLoadingState);
  const successState = useSelector(getSuccessState);
  const errorState = useSelector(getErrorState);
  const errorMessage = useSelector(getErrorStateMessage);

  const selectedVariantId = selectedVariant.id;

  /* Check availability based on the variant selection from liquid obj */
  const isVariantAvailable = window.adminPersonalization?.isAdminPanel
    ? true
    : window.productMetaData?.product?.variants?.find(
        (variant) => variant.id === selectedVariantId
      )?.available;

  const onClick = (step) => {
    dispatch(
      stepsActions.changeCurrentStep({ step, stepData: filteredStepper[step] })
    );
  };

  const disableFun = () => {
    let keyName = currentStepData?.label || currentStepData?.name;
    if (useDesignElement) {
      const requiredSubFieldForDesignElements =
        currentStepData?.personalization_options?.filter((item) => {
          if (item?.type === "input") {
            return inputValues[item?.name]?.length > item.max_count;
          } else if (item?.type === "dropdown") {
            return item?.attributes?.is_required === POSITIVE_BOOLEAN_STRING
              ? !inputValues?.[item?.name] ||
                  inputValues?.[item?.name]?.length === 0
              : false;
          }
        }) || [];

      /* Disable when inputs length are greater than required */
      if (requiredSubFieldForDesignElements?.length > 0) return true;

      return (
        (Object.keys(errorData)?.length > 0
          ? Boolean(Object.keys(errorData)?.includes(currentStepData?.label))
          : false) ||
        currentStepData?.personalization_options?.filter((item) => {
          if (["input", "dropdown"].includes(item?.type)) {
            return (
              item?.attributes?.is_required === POSITIVE_BOOLEAN_STRING &&
              (!inputValues?.[item?.name] ||
                inputValues?.[item?.name]?.length === 0)
            );
          } else return false;
        })?.length > 0 ||
        !isVariantAvailable ||
        successState ||
        errorState
      );
    } else {
      /* Customized Text validations */
      return (
        (Object.keys(errorData)?.length > 0
          ? Boolean(Object.keys(errorData)?.includes(currentStepData?.label))
          : false) ||
        (currentStepData?.isRequired &&
          (!inputValues?.[keyName] || inputValues?.[keyName]?.length === 0) &&
          ["input", "dropdown"].includes(currentStepData?.fieldtype)) ||
        !isVariantAvailable ||
        successState ||
        errorState
      );
    }
  };

  const handleAddToCart = async () => {
    let svgElement = document.querySelector("#image-container svg");
    let finalSvgElement = svgElement?.cloneNode();
    if (finalSvgElement) {
      finalSvgElement.innerHTML = svgElement ? svgElement.innerHTML : "";
      finalSvgElement.style.backgroundImage = "";
    }

    const base64PreviewImg = svgElement
      ? await svgToBase64(Buffer, svgElement)
      : "";
    const base64FinalImg = finalSvgElement
      ? await svgToBase64(Buffer, finalSvgElement)
      : "";

    if (window.adminPersonalization?.isAdminPanel) {
      /* Set Loading state  */
      dispatch(stepsActions.setLoadingState(true));
      /* Admin Panel Create JSON */
      const saveChanges = new CustomEvent("saveChanges", {
        detail: { inputValues, base64PreviewImg, base64FinalImg },
      });
      dispatchEvent(saveChanges);
    } else {
      /* Add to cart product */
      dispatch({
        type: ADD_TO_CART,
        payload: {
          preview_image: base64PreviewImg,
          final_image: base64FinalImg,
          variantId: selectedVariant?.id,
        },
      });
      /* Set Loading state  */
      dispatch(stepsActions.setLoadingState(true));
    }
  };

  return (
    <>
      {!isVariantAvailable ? (
        <span
          style={{
            color: "#f2994a",
          }}
        >
          Selected Variant is out of stock
        </span>
      ) : null}
      {successState ? (
        <span
          style={{
            color: "#27AE60",
          }}
        >
          Item added to your cart
        </span>
      ) : null}
      {errorState ? (
        <span
          style={{
            color: "#f2994a",
          }}
        >
          {errorMessage}
        </span>
      ) : null}
      <div className="footer-wrapper">
        <Button
          variant="contained"
          className="footer-prev-btn"
          disabled={currentStep === 0}
          onClick={() => {
            onClick(currentStep - 1);
            dispatch(
              stepsActions.setErrorState({ error: false, errorMessage: "" })
            );
          }}
        >
          Prev
        </Button>
        <Button
          variant="contained"
          className="footer-next-btn"
          disabled={disableFun()}
          onClick={() =>{
            if(loadingState || successState) return
            currentStep === totalStep - 1 
              ? handleAddToCart()
              : onClick(currentStep + 1)
          }
          }
        >
          {loadingState ? (
            <CircularProgress size={25} color="inherit" />
          ) : successState ? (
            <DoneIcon />
          ) : currentStep === totalStep - 1 ? (
            window?.adminPersonalization?.isAdminPanel ? (
              "Save changes"
            ) : (
              "Add to cart"
            )
          ) : (
            "Next"
          )}
        </Button>
      </div>
    </>
  );
};
