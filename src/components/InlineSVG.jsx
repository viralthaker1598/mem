import React, { useEffect, useState } from "react";
import { SHOP_NAME } from "../constants";

export const InlineSVG = ({ url }) => {
  const [svg, setSvg] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isErrored, setIsErrored] = useState(false);

  useEffect(() => {
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        shopname:
          import.meta.env.mode === "production"
            ? window.shopUrl.replace("https://", "")
            : SHOP_NAME,
      },
    })
      .then((res) => res.text())
      .then((res) => console.log(res))
      .catch((error) => console.log(error))
      .then(() => setIsLoaded(true));
  }, [url]);

  return (
    <div
      className={`svgInline svgInline--${isLoaded ? "loaded" : "loading"} ${
        isErrored ? "svgInline--errored" : ""
      }`}
      dangerouslySetInnerHTML={{ __html: svg }}
    />
  );
};
