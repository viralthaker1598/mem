import { Box, Button, Modal } from "@mui/material";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Preview from "./components/Preview";
import { getModalState, modalActions } from "./redux/slice/modal";
import { getPreviewData, previewActions } from "./redux/slice/preview";
import { stepsActions } from "./redux/slice/steps";
import { FETCH_PRODUCT_METADATA } from "./redux/types/previewType";
import "./styles/index.scss";
import CloseIcon from "@mui/icons-material/Close";
import { LOADING_STATE, POSITIVE_BOOLEAN_STRING } from "./constants";
import { findAscendingTag } from "./utils";

function App() {
  const dispatch = useDispatch();
  const modalState = useSelector(getModalState);
  const previewData = useSelector(getPreviewData);
  const isContentLoaded = Boolean(previewData !== LOADING_STATE);

  let isCustomizationAvailable = Boolean(previewData);
  if (isCustomizationAvailable) {
    /* double check */
    isCustomizationAvailable = Boolean(
      previewData.customize_text ||
        previewData.design_elements?.filter(
          (design_element) =>
            design_element?.attributes?.is_enable === POSITIVE_BOOLEAN_STRING
        ).length > 0
    );
  }

  useEffect(() => {
    const isContentLoaded = Boolean(previewData !== LOADING_STATE);
    let isCustomizationAvailable = Boolean(previewData);

    if (isContentLoaded && !isCustomizationAvailable) {
      /* if personalization not available */
      if (window.atcButton) window.atcButton.style.display = "block";
    } else if (isContentLoaded && isCustomizationAvailable) {
      /* enable personalization button */
      const rootDiv = document.querySelector("#preview-root-app-container");

      let memWidget = window.mem_widget;
      const mainClass = document.querySelector(`.${memWidget?.main_class}`);

      if (memWidget && memWidget?.main_class && mainClass) {
        //mainClass && mainClass.appendChild(rootDiv);
      } else {
        /* === Insert under variant selector form === */
        const forms = document.querySelectorAll('form[action^="/cart/add"]');
        let memParentNode = document.body;
        if (forms.length > 1) {
          memParentNode = document.querySelector(
            'form[action^="/cart/add"][id^="product-form-template"]'
          );
        } else memParentNode = forms[0];

        memParentNode = memParentNode || document.body;
        const monoDiv = document.querySelector("#preview-root-app-container");
        memParentNode.removeAttribute("novalidate");
        const selector = memParentNode.querySelector('[name^="id"]');
        const el = findAscendingTag(selector, "FORM");
        memParentNode?.insertBefore(monoDiv, el);
      }

      if (rootDiv) rootDiv.style.display = "block";
    }
  }, [JSON.stringify(previewData || {})]);

  const getButtonVariantType = (type) => {
    let variantType = "text";
    switch (type) {
      case "outline":
        variantType = "outlined";
        break;
      case "fill":
        variantType = "contained";
        break;
      case "text":
        variantType = "text";
        break;
      default:
        variantType = "text";
        break;
    }
    return variantType;
  };

  const handleOpenModal = () => {
    dispatch(modalActions.open());
    let qty = parseInt(
      document.querySelector("input[type=number][name=quantity]")?.value || 1
    );
    dispatch(stepsActions.changeSelectedQuantity(qty));
  };
  const handleCloseModal = () => {
    /* Dispatch custom event of Close modal */
    const modalCloseEvent = new CustomEvent("modalClose", {
      detail: { open: false },
    });
    dispatchEvent(modalCloseEvent);
    dispatch(modalActions.close());
    dispatch(stepsActions.changeCurrentStep({ step: 0, stepData: {} }));
    dispatch(stepsActions.changeTotalSteps({ total: 0, stepData: {} }));
  };

  window.handlePreviewModalOpen = handleOpenModal;
  window.handlePreviewModalClose = () => {
    handleCloseModal();
    dispatch(stepsActions.resetStepsState());
  };

  useEffect(() => {
    dispatch({
      type: FETCH_PRODUCT_METADATA,
      payload: {},
    });

    window.initPersonalize = () => {
      dispatch({
        type: FETCH_PRODUCT_METADATA,
        payload: {},
      });
    };
    window.setSelectedVariant = (variant) => {
      document.querySelectorAll("[name^=id]").forEach((ele) => {
        ele.value = variant?.id;
      });
      dispatch(previewActions.setVariant(variant));
    };
    window.setProperties = (properties) =>
      dispatch(stepsActions.setInputValues(properties));
  }, []);

  return (
    <div className="preview-root-app-wrapper">
      {window.mem_widget && window.mem_widget?.main_class ? (
        <Button
          onClick={handleOpenModal}
          variant={
            getButtonVariantType(window.mem_widget?.personalize_button?.type) ||
            "contained"
          }
          style={{
            color: window.mem_widget?.personalize_button?.text_color || "black",
            fontSize:
              window.mem_widget?.personalize_button?.text_size + "px" || "16px",
            fontStyle:
              window.mem_widget?.personalize_button?.font_style || "normal",
            borderColor:
              window.mem_widget?.personalize_button?.outline_color || "black",
            backgroundColor:
              window.mem_widget?.personalize_button?.type === "fill"
                ? window.mem_widget?.personalize_button?.fill_color
                : "initial",
          }}
          disabled={!isContentLoaded || !isCustomizationAvailable}
          fullWidth
        >
          {window.mem_widget?.personalize_button?.text || "Personalize"}
        </Button>
      ) : (
        <Button
          onClick={handleOpenModal}
          variant="outlined"
          style={{
            fontWeight: "bold",
            fontStyle: "italic",
            fontSize: "inherit",
          }}
          disabled={!isContentLoaded || !isCustomizationAvailable}
          fullWidth
        >
          {isContentLoaded
            ? isCustomizationAvailable
              ? "Personalize"
              : "Personalization not available"
            : LOADING_STATE}
        </Button>
      )}

      <Modal
        className="preview-personalization-modal"
        open={modalState.open}
        onClose={handleCloseModal}
      >
        <div className="preview-personalization-wrapper">
          <div className="preview-modal-box-wrapper">
            <Box
              sx={{
                bgcolor: "background.paper",
                border: "2px solid white",
                boxShadow: 24,
                p: 4,
                borderRadius: "10px",
              }}
              className="preview-modal-box"
            >
              <div className="modal-header">
                <span>Update Personalization</span>
                <CloseIcon className="pointer" onClick={handleCloseModal} />
              </div>
              <div className="modal-preview">
                {!isCustomizationAvailable ? (
                  <span
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      fontSize: "18px",
                      color: "#F2994A",
                    }}
                  >
                    Personalization not available
                  </span>
                ) : (
                  <Preview />
                )}
              </div>
            </Box>
          </div>
        </div>
      </Modal>
    </div>
  );
}

export default App;
