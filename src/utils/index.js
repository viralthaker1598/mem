import { POSITIVE_BOOLEAN_STRING } from "../constants";

export const refineDesignAndCustomText = (customText, designElements) => {
  const allowedDesignElements = designElements.filter((item) => {
    return (
      item.attributes.is_enable === POSITIVE_BOOLEAN_STRING &&
      item.personalization_options?.length > 0
    );
  });
  return allowedDesignElements.length > 0 ? allowedDesignElements : customText;
};

export const convertSvgToImg = (Buffer, svgElement) => {
  /* Serialize the svg to string */
  let svgString = new XMLSerializer().serializeToString(svgElement);
  /* Using btoa to convert the svg to base64 */
  //let base64 = Buffer.from(svgString).toString("base64");
  let base64Image =
    '<img src="data:image/svg+xml;base64,' +
    btoa(unescape(encodeURIComponent(svgString))) +
    '"/>';
  return base64Image;
};

export const svgToBase64 = async (Buffer, svgElement) => {
  /* Serialize the svg to string */
  let svgString = new XMLSerializer().serializeToString(svgElement);
  /* SVG data URL from SVG string */
  var svgData = await Buffer.from(svgString).toString("base64");

  return svgData;
};

export const findAscendingTag = (selector, tagName = "FORM") => {
  let prevNode = selector;
  while (selector?.parentNode) {
    prevNode = selector;
    selector = selector.parentNode;
    if (selector.tagName === tagName) {
      break;
    }
  }
  return prevNode;
};
