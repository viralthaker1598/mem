export const script = `/* Version: 2.0.44 - June 23, 2022 10:49:45 */
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
	try {
		var info = gen[key](arg);
		var value = info.value;
	} catch (error) {
		reject(error);
		return;
	}

	if (info.done) {
		resolve(value);
	} else {
		Promise.resolve(value).then(_next, _throw);
	}
}

function _asyncToGenerator(fn) {
	return function () {
		var self = this,
			args = arguments;
		return new Promise(function (resolve, reject) {
			var gen = fn.apply(self, args);

			function _next(value) {
				asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
			}

			function _throw(err) {
				asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
			}

			_next(undefined);
		});
	};
}

function _classCallCheck(instance, Constructor) {
	if (!(instance instanceof Constructor)) {
		throw new TypeError("Cannot call a class as a function");
	}
}

function _defineProperty(obj, key, value) {
	if (key in obj) {
		Object.defineProperty(obj, key, {
			value: value,
			enumerable: true,
			configurable: true,
			writable: true,
		});
	} else {
		obj[key] = value;
	}

	return obj;
}

function applyPersonalizationToSvg(svg, customization) {
	// only apply customizations that exist
	svg = applyPersonalizationSelectsToSvg(svg, customization.selectMap);
	svg = applyPersonalizationTextsToSvg(svg, customization.textMap);
	return svg;
}
function applyPersonalizationTextsToSvg(svg, textMap) {
	/**
	 * WARNING: Could be more values than containers
	 * 1. Extract custom text container ids from svg, in order
	 * 2. For each value and correlating container ID, apply customization text to svg
	 */
	// Reverse values because for some reason, monofields 1,2,3 go Top, Middle, Bottom,
	// while the SVG custom fields 1,2,3 go Bottom, Middle, Top
	var keys = Object.keys(textMap);
	Object.keys(textMap)
		.filter(function (k) {
			return keys.includes(k);
		})
		.forEach(function (k) {
			svg = applyPersonalizationTextToSvg(svg, k, textMap[k]);
		});
	return svg;
}
function applyPersonalizationSelectsToSvg(svg, selectMap) {
	svg = clearSvgSelection(svg);
	Object.keys(selectMap).forEach(function (k) {
		applyPersonalizationSelectToSvg(svg, k, selectMap[k]);
	});
	return svg;
}
function clearSvgSelection(svg) {
	var variantGroups = Array.from(svg.querySelectorAll("g.variant"));
	variantGroups.forEach(function (svgGroup) {
		svgGroup.querySelectorAll("g").forEach(function (n) {
			n.style.display = "none";
		});
	});
	return svg;
}
function applyPersonalizationTextToSvg(svg, position, value) {
	var container = svg.getElementById(position);

	if (container) {
		if (containerIsCurved(container)) {
			svg = applyCurvedText(svg, value, position);
		} else if (containerIsStraight(container)) {
			svg = applyStraightText(svg, value, position);
		}
	}

	return svg;
}
function containerIsCurved(text) {
	return text.getElementsByTagName("textPath").length > 0;
}
function containerIsStraight(text) {
	return text.getElementsByTagName("rect").length > 0;
}
function applyCurvedText(svg, value, containerId) {
	var lengthAdjust = "spacingAndGlyphs"; // Get the Custom_Text node

	var container = svg.getElementById(containerId); // Get the <text> node

	var containerText = container.getElementsByTagName("text")[0]; // Get the <textPath> node

	var containerTextPath = containerText.getElementsByTagName("textPath")[0]; // If it exists

	if (containerTextPath) {
		var containerTextPathId = "".concat(containerId, "_Path");
		containerTextPath.removeAttribute("textLength");
		containerTextPath.removeAttribute("startOffset");
		containerTextPath.removeAttribute("text-anchor");
		containerTextPath.setAttribute("id", containerTextPathId); // Get the <tspan> node

		var tspan = containerTextPath.getElementsByTagName("tspan")[0]; // Set the text content of the <tspan> node to the custom text

		if (tspan) tspan.innerHTML = value; // Set the lengthAdjust manually

		containerTextPath.setAttribute("lengthAdjust", lengthAdjust); // Calculate the max curve length (custom text can't exceed this)

		var maxCurveLength = container.getElementsByTagName("path")[0].getTotalLength(); // Get the computed text length from the <textPath> node (should give us the length of the custom text)

		var currentTextLength = containerTextPath.getComputedTextLength(); // if the currentTextLength is greater that the maxCurveLength, set the textLength to be the maxCurveLength

		if (currentTextLength > maxCurveLength) {
			containerTextPath.setAttribute("textLength", "".concat(maxCurveLength));
			containerTextPath.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, maxCurveLength);
			containerTextPath.removeAttribute("startOffset");
			containerTextPath.removeAttribute("text-anchor");
		} else {
			containerTextPath.setAttribute("startOffset", "50%");
			containerTextPath.setAttribute("text-anchor", "middle");
			containerTextPath.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, currentTextLength);
		}

		if (isMobile()) {
			handleMobile(containerTextPathId, maxCurveLength);
		}
	}

	return svg;
}
function isMobile() {
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}
function handleMobile(containerTextPathId, maxCurveLength) {
	// Hack for handling mobile devices. Essentially use an interval every 2 ms to check for changed to textLength, since it doesnt update fast enough on mobile.
	var limit = 20;
	var iter = 0;
	var interval = setInterval(function () {
		// var container = document.getElementById(container_id);
		// Get the <text> node
		// Get the <textPath> node
		var containerTextPath = document.querySelector("#".concat(containerTextPathId));

		if (containerTextPath) {
			var currentTextLength = containerTextPath.getComputedTextLength(); // var max_curve_length = container
			//   .getElementsByTagName("path")[0]
			//   .getTotalLength();

			if (currentTextLength > maxCurveLength) {
				containerTextPath.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, maxCurveLength);
				containerTextPath.removeAttribute("startOffset");
				containerTextPath.removeAttribute("text-anchor");
			} else {
				containerTextPath.setAttribute("startOffset", "50%");
				containerTextPath.setAttribute("text-anchor", "middle");
				containerTextPath.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, currentTextLength);
			}
		}

		iter++;
		if (iter >= limit) clearInterval(interval);
	}, 100);
}
function applyStraightText(svg, value, contId) {
	//Get the <svg> node, Custom_Text_<i> node, <text> node, and <rect> node
	var container = svg.querySelector("#" + contId);

	if (container) {
		var text = container.getElementsByTagName("text")[0];
		var rect = container.getElementsByTagName("rect")[0];
		var lengthAdjust = "spacingAndGlyphs"; // Set the text content.

		text.textContent = value; // Hack to make sure this always stays 1 for adjustment.

		if (text.transform && text.transform.baseVal && text.transform.baseVal.getItem(0)) {
			text.transform.baseVal.getItem(0).matrix.a = 1;
		}

		var customTextRectXVal = rect.getAttribute("x");

		if (customTextRectXVal) {
			// Get the computed (whitespace removed) text length and adjusted (with whitespace) text length
			var computedTextLength = text.getComputedTextLength();
			var adjustedTextLength = text.textLength.baseVal.value; // If whitespace length is greater than no whitespace length, set it to no whitespace length.

			if (adjustedTextLength > computedTextLength) {
				text.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, computedTextLength);
			} // if the computed text length is GREATER THAN the <rect> node width:
			// remove any leftover x coordinates,
			// AND set the matrix e coordinate to the rect_element_x_value

			var rectWidth = rect.width.baseVal.value;
			var rectXVal = parseInt(customTextRectXVal);

			if (computedTextLength >= rectWidth) {
				text.removeAttribute("x");
				text.setAttribute("lengthAdjust", lengthAdjust);
				text.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, rectWidth);
				if (text.transform.baseVal.getItem(0)) text.transform.baseVal.getItem(0).matrix.e = rectXVal;
			} else {
				// else (if the computed text length is LESS THAN the <rect> node width)
				// The text now must set the x attribute to be the same as the rect_element_x_value
				// The e matrix coordinate (translate x value) must be half the rect width, minus half the computed text length
				// Ex:
				// |-----------------------------| rect width
				// |---------------| half the rect width (centered the offset)
				//                 |------------------| computed text length (if matrix e is set to just half the rect width)
				//        |------------------| computed text length
				//                            (if matrix e set to half the rect width MINUS half the computed text length)
				var halfRectWidth = rectWidth / 2;
				var halfTextLen = computedTextLength / 2;
				var centerECoord = halfRectWidth - halfTextLen;

				if (text.transform.baseVal.getItem(0)) {
					text.transform.baseVal.getItem(0).matrix.e = centerECoord;
				}

				text.setAttribute("x", "".concat(rectXVal));
				text.removeAttribute("lengthAdjust");
				text.textLength.baseVal.newValueSpecifiedUnits(SVGLength.SVG_LENGTHTYPE_PX, computedTextLength);
			}
		}
	}

	return svg;
}
function applyPersonalizationSelectToSvg(svg, position, value) {
	var element = svg.querySelector("g#".concat(position, " > g[id^='").concat(value, "']"));

	if (element) {
		element.style.display = "block";
		element.style.fill = "url(#pattern)";
		element.style.filter = "url(#dropshadow)";
		element.querySelectorAll("*").forEach(function (e) {
			e.style.display = "block";
		});
	}

	return svg;
}

var stylers = {
	// Text colors.
	black: "color: black",
	red: "color: red",
	green: "color: green",
	blue: "color: blue",
	yellow: "color: yellow",
	magenta: "color: magenta",
	cyan: "color: cyan",
	white: "color: white",
	gray: "color: gray",
	gold: "color: gold",
	// Background colors.
	bgBlack: "background-color: black",
	bgRed: "background-color: red",
	bgGreen: "background-color: green",
	bgBlue: "background-color: blue",
	bgYellow: "background-color: yellow",
	bgMagenta: "background-color: magenta",
	bgCyan: "background-color: cyan",
	bgWhite: "background-color: white",
	bgGray: "background-color: gray",
	bgGold: "background-color: gold",
	// Text modifiers.
	bold: "font-weight: bold",
	lighter: "font-weight: lighter",
	italic: "font-style: italic",
	strikethrough: "text-decoration: line-through",
	underline: "text-decoration: underline",
	large: "font-size: 16px",
	larger: "font-size: 22px",
	largest: "font-size: 26px",
	massive: "font-size: 36px",
	// Block modifiers.
	padded: "display: inline-block ; padding: 4px 6px",
	rounded: "display: inline-block ; border-radius: 4px",
};
var error = function error(log) {
	if (typeof log === "string") {
		console.error("%c MyEasySuite Live Preview:  ".concat(log), "".concat(stylers.bgWhite, ";").concat(stylers.black, ";"));
	} else {
		console.error("%c MyEasySuite Live Preview:", "".concat(stylers.bgWhite, ";").concat(stylers.black, ";"), log);
	}
};
var log = function log(_log) {
	return false;
	if (typeof _log === "string") {
		console.log(
			"%c MyEasySuite Live Preview:" + "%c ".concat(_log),
			"".concat(stylers.bgWhite, ";").concat(stylers.black, ";"),
			"".concat(stylers.bgWhite, ";").concat(stylers.black, ";").concat(stylers.bold)
		);
	} else {
		console.log("%c MyEasySuite Live Preview: ", "".concat(stylers.bgWhite, ";").concat(stylers.black, ";"), _log);
	}
};

function injectLivePreviewImageIntoMainProductImageContainer() {
	var _document;

	var productImageContainer;
	var flickityViewport = document.querySelector(".flickity-viewport");
	var ttProductSingleImg = document.querySelector(".tt-product-single-img img");
	var twenty60 = document.querySelector(".product-images__image .slick-slide.slick-current.slick-active img");
	var slickImg = document.querySelector(".slick-slide.slick-current.slick-active img");
	var photoItemsMain = document.querySelector(".product__photo-container:not(.hide) img");
	var productImageElement = document.querySelector(".product-main-slide.is-selected img");
	var productSingle = document.querySelector(".product-single img");

	if (productSingle && productSingle.offsetHeight) {
		productImageContainer = productSingle.parentElement;
	}

	var productMainImageElement = document.querySelector(".product__media-item.is-active .product__media img");
	var productImgModalElement = document.querySelector(".product__media-item--previewElement.is-active .product__media");
	var productSingleMediaGroup = document.querySelector(".product-single__media-group-wrapper img");
	var mobile = "#mobile-product-photos img";
	var desktop = "#product-photos img";
	var keychain = ".product-image-container";
	var selector = isMobile() ? mobile : desktop;
	var braveAmericanProdImg = document.querySelector(selector);

	if (!braveAmericanProdImg) {
		braveAmericanProdImg = document.querySelector(keychain);
	}

	var primaryMediaContainerImg = document.querySelector(".primary-media-container.active img");

	if(productImgModalElement){
		productImageContainer = productImgModalElement;
		log("PREVIEW: MODAL CONTAINER IMG FOUND LOADING SVG INTO IT.");
	} else if (primaryMediaContainerImg) {
		productImageContainer = primaryMediaContainerImg.parentElement;
	} else if (braveAmericanProdImg) {
		productImageContainer = braveAmericanProdImg.parentElement;
	} else if (productSingleMediaGroup) {
		var _productImageContaine;

		productImageContainer = productSingleMediaGroup.parentElement;
		(_productImageContaine = productImageContainer) === null || _productImageContaine === void 0
			? void 0
			: _productImageContaine.style.setProperty("position", "relative");
	} else if (productImageElement) {
		var _productImageContaine2;

		productImageContainer = productImageElement.parentElement;
		(_productImageContaine2 = productImageContainer) === null || _productImageContaine2 === void 0
			? void 0
			: _productImageContaine2.style.setProperty("position", "relative");
	} else if (twenty60 && twenty60.offsetHeight) {
		var _productImageContaine3;

		productImageContainer = twenty60.parentElement;
		(_productImageContaine3 = productImageContainer) === null || _productImageContaine3 === void 0
			? void 0
			: _productImageContaine3.style.setProperty("position", "relative");
	} else if (photoItemsMain && photoItemsMain.offsetHeight) {
		productImageContainer = photoItemsMain.parentElement;
	} else if (ttProductSingleImg && ttProductSingleImg.offsetHeight) {
		log("TT_PRODUCT: Found TT Product container. Inserting Live Preview in IMG parent");
		productImageContainer = ttProductSingleImg.parentElement;
	} else if (flickityViewport) {
		log("FLICKITY: Found Flickity viewport container. Inserting Live Preview in is-selected.");
		productImageContainer = flickityViewport.querySelector(".is-selected");
	} else if (slickImg && slickImg.offsetHeight) {
		log("SLICK_IMG: Found Slick viewport IMG container. Inserting Live Preview in IMG parent.");
		productImageContainer = slickImg.parentElement;
	} else if (productMainImageElement && productMainImageElement.offsetHeight) {
		productImageContainer = productMainImageElement.parentElement;
	} else {
		log("DEFAULT: No special cases found: looking up all product images and finding best fit.");
		productImageContainer = Array.from(document.querySelectorAll("img"))
			.filter(function (i) {
				return i.currentSrc && i.currentSrc.includes("/products/");
			})
			.filter(function (i) {
				return i.offsetHeight != 0;
			})[0].parentNode;
	}

	var memCanvasContainer = (_document = document) === null || _document === void 0 ? void 0 : _document.querySelector("#mem-canvas-container");
	if (memCanvasContainer) memCanvasContainer.remove();

	if (productImageContainer) {
		var memCanvasContainerDiv = document.createElement("div");

		if (document.URL.includes("momavo")) {
			memCanvasContainerDiv.style.position = "relative";
		} else {
			memCanvasContainerDiv.style.position = "absolute";
		}

		if (document.URL.includes("pipelineproud")) {
			var productPhoto = document.querySelector("#productPhoto");
			if (productPhoto) productPhoto.style.position = "relative";
		}

		var dawnEnabled = document.querySelector(".product__media");

		if (dawnEnabled) {
			memCanvasContainerDiv.style.position = "absolute";
		}

		memCanvasContainerDiv.style.top = "0";
		memCanvasContainerDiv.style.left = "0";
		memCanvasContainerDiv.style.height = "100%";
		memCanvasContainerDiv.style.width = "100%";
		memCanvasContainerDiv.style.zIndex = "1003";

		if (document.URL.includes("familygiftmall")) {
			memCanvasContainerDiv.style.zIndex = "100";
		}

		memCanvasContainerDiv.style.backgroundColor = "white";
		memCanvasContainerDiv.id = "mem-canvas-container";
		memCanvasContainerDiv.style.display = "flex";
		memCanvasContainerDiv.style.alignItems = "center";
		memCanvasContainerDiv.style.justifyContent = "center";
		memCanvasContainerDiv = productImageContainer.insertBefore(memCanvasContainerDiv, productImageContainer.firstChild);
		if(document.querySelector('#preview-root-app-container')){
		productImageContainer.style.height = memCanvasContainerDiv.getBoundingClientRect().width + 35 + "px"
		}
		if (window.location.origin.includes("thescarletwagon")) {
			var scrollComponent = document.querySelector("slider-component");
			scrollComponent.scrollToStart();
		}

		return memCanvasContainerDiv;
	}
}

var commonjsGlobal =
	typeof globalThis !== "undefined"
		? globalThis
		: typeof window !== "undefined"
		? window
		: typeof global !== "undefined"
		? global
		: typeof self !== "undefined"
		? self
		: {};

var pubsub = { exports: {} };

/**
 * Copyright (c) 2010,2011,2012,2013,2014 Morgan Roderick http://roderick.dk
 * License: MIT - http://mrgnrdrck.mit-license.org
 *
 * https://github.com/mroderick/PubSubJS
 */

(function (module, exports) {
	(function (root, factory) {
		var PubSub = {};
		root.PubSub = PubSub;
		factory(PubSub);
		// CommonJS and Node.js module support
		{
			if (module !== undefined && module.exports) {
				exports = module.exports = PubSub; // Node.js specific "module.exports"
			}
			exports.PubSub = PubSub; // CommonJS module 1.1.1 spec
			module.exports = exports = PubSub; // CommonJS
		}
	})((typeof window === "object" && window) || commonjsGlobal, function (PubSub) {
		var messages = {},
			lastUid = -1,
			ALL_SUBSCRIBING_MSG = "*";

		function hasKeys(obj) {
			var key;

			for (key in obj) {
				if (Object.prototype.hasOwnProperty.call(obj, key)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Returns a function that throws the passed exception, for use as argument for setTimeout
		 * @alias throwException
		 * @function
		 * @param { Object } ex An Error object
		 */
		function throwException(ex) {
			return function reThrowException() {
				throw ex;
			};
		}

		function callSubscriberWithDelayedExceptions(subscriber, message, data) {
			try {
				subscriber(message, data);
			} catch (ex) {
				setTimeout(throwException(ex), 0);
			}
		}

		function callSubscriberWithImmediateExceptions(subscriber, message, data) {
			subscriber(message, data);
		}

		function deliverMessage(originalMessage, matchedMessage, data, immediateExceptions) {
			var subscribers = messages[matchedMessage],
				callSubscriber = immediateExceptions ? callSubscriberWithImmediateExceptions : callSubscriberWithDelayedExceptions,
				s;

			if (!Object.prototype.hasOwnProperty.call(messages, matchedMessage)) {
				return;
			}

			for (s in subscribers) {
				if (Object.prototype.hasOwnProperty.call(subscribers, s)) {
					callSubscriber(subscribers[s], originalMessage, data);
				}
			}
		}

		function createDeliveryFunction(message, data, immediateExceptions) {
			return function deliverNamespaced() {
				var topic = String(message),
					position = topic.lastIndexOf(".");

				// deliver the message as it is now
				deliverMessage(message, message, data, immediateExceptions);

				// trim the hierarchy and deliver message to each level
				while (position !== -1) {
					topic = topic.substr(0, position);
					position = topic.lastIndexOf(".");
					deliverMessage(message, topic, data, immediateExceptions);
				}

				deliverMessage(message, ALL_SUBSCRIBING_MSG, data, immediateExceptions);
			};
		}

		function hasDirectSubscribersFor(message) {
			var topic = String(message),
				found = Boolean(Object.prototype.hasOwnProperty.call(messages, topic) && hasKeys(messages[topic]));

			return found;
		}

		function messageHasSubscribers(message) {
			var topic = String(message),
				found = hasDirectSubscribersFor(topic) || hasDirectSubscribersFor(ALL_SUBSCRIBING_MSG),
				position = topic.lastIndexOf(".");

			while (!found && position !== -1) {
				topic = topic.substr(0, position);
				position = topic.lastIndexOf(".");
				found = hasDirectSubscribersFor(topic);
			}

			return found;
		}

		function publish(message, data, sync, immediateExceptions) {
			message = typeof message === "symbol" ? message.toString() : message;

			var deliver = createDeliveryFunction(message, data, immediateExceptions),
				hasSubscribers = messageHasSubscribers(message);

			if (!hasSubscribers) {
				return false;
			}

			if (sync === true) {
				deliver();
			} else {
				setTimeout(deliver, 0);
			}
			return true;
		}

		/**
		 * Publishes the message, passing the data to it's subscribers
		 * @function
		 * @alias publish
		 * @param { String } message The message to publish
		 * @param {} data The data to pass to subscribers
		 * @return { Boolean }
		 */
		PubSub.publish = function (message, data) {
			return publish(message, data, false, PubSub.immediateExceptions);
		};

		/**
		 * Publishes the message synchronously, passing the data to it's subscribers
		 * @function
		 * @alias publishSync
		 * @param { String } message The message to publish
		 * @param {} data The data to pass to subscribers
		 * @return { Boolean }
		 */
		PubSub.publishSync = function (message, data) {
			return publish(message, data, true, PubSub.immediateExceptions);
		};

		/**
		 * Subscribes the passed function to the passed message. Every returned token is unique and should be stored if you need to unsubscribe
		 * @function
		 * @alias subscribe
		 * @param { String } message The message to subscribe to
		 * @param { Function } func The function to call when a new message is published
		 * @return { String }
		 */
		PubSub.subscribe = function (message, func) {
			if (typeof func !== "function") {
				return false;
			}

			message = typeof message === "symbol" ? message.toString() : message;

			// message is not registered yet
			if (!Object.prototype.hasOwnProperty.call(messages, message)) {
				messages[message] = {};
			}

			// forcing token as String, to allow for future expansions without breaking usage
			// and allow for easy use as key names for the 'messages' object
			var token = "uid_" + String(++lastUid);
			messages[message][token] = func;

			// return token for unsubscribing
			return token;
		};

		PubSub.subscribeAll = function (func) {
			return PubSub.subscribe(ALL_SUBSCRIBING_MSG, func);
		};

		/**
		 * Subscribes the passed function to the passed message once
		 * @function
		 * @alias subscribeOnce
		 * @param { String } message The message to subscribe to
		 * @param { Function } func The function to call when a new message is published
		 * @return { PubSub }
		 */
		PubSub.subscribeOnce = function (message, func) {
			var token = PubSub.subscribe(message, function () {
				// before func apply, unsubscribe message
				PubSub.unsubscribe(token);
				func.apply(this, arguments);
			});
			return PubSub;
		};

		/**
		 * Clears all subscriptions
		 * @function
		 * @public
		 * @alias clearAllSubscriptions
		 */
		PubSub.clearAllSubscriptions = function clearAllSubscriptions() {
			messages = {};
		};

		/**
		 * Clear subscriptions by the topic
		 * @function
		 * @public
		 * @alias clearAllSubscriptions
		 * @return { int }
		 */
		PubSub.clearSubscriptions = function clearSubscriptions(topic) {
			var m;
			for (m in messages) {
				if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
					delete messages[m];
				}
			}
		};

		/**
       Count subscriptions by the topic
     * @function
     * @public
     * @alias countSubscriptions
     * @return { Array }
    */
		PubSub.countSubscriptions = function countSubscriptions(topic) {
			var m;
			// eslint-disable-next-line no-unused-vars
			var token;
			var count = 0;
			for (m in messages) {
				if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
					for (token in messages[m]) {
						count++;
					}
					break;
				}
			}
			return count;
		};

		/**
       Gets subscriptions by the topic
     * @function
     * @public
     * @alias getSubscriptions
    */
		PubSub.getSubscriptions = function getSubscriptions(topic) {
			var m;
			var list = [];
			for (m in messages) {
				if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
					list.push(m);
				}
			}
			return list;
		};

		/**
		 * Removes subscriptions
		 *
		 * - When passed a token, removes a specific subscription.
		 *
		 * - When passed a function, removes all subscriptions for that function
		 *
		 * - When passed a topic, removes all subscriptions for that topic (hierarchy)
		 * @function
		 * @public
		 * @alias subscribeOnce
		 * @param { String | Function } value A token, function or topic to unsubscribe from
		 * @example // Unsubscribing with a token
		 * var token = PubSub.subscribe('mytopic', myFunc);
		 * PubSub.unsubscribe(token);
		 * @example // Unsubscribing with a function
		 * PubSub.unsubscribe(myFunc);
		 * @example // Unsubscribing from a topic
		 * PubSub.unsubscribe('mytopic');
		 */
		PubSub.unsubscribe = function (value) {
			var descendantTopicExists = function (topic) {
					var m;
					for (m in messages) {
						if (Object.prototype.hasOwnProperty.call(messages, m) && m.indexOf(topic) === 0) {
							// a descendant of the topic exists:
							return true;
						}
					}

					return false;
				},
				isTopic = typeof value === "string" && (Object.prototype.hasOwnProperty.call(messages, value) || descendantTopicExists(value)),
				isToken = !isTopic && typeof value === "string",
				isFunction = typeof value === "function",
				result = false,
				m,
				message,
				t;

			if (isTopic) {
				PubSub.clearSubscriptions(value);
				return;
			}

			for (m in messages) {
				if (Object.prototype.hasOwnProperty.call(messages, m)) {
					message = messages[m];

					if (isToken && message[value]) {
						delete message[value];
						result = value;
						// tokens are unique, so we can just stop here
						break;
					}

					if (isFunction) {
						for (t in message) {
							if (Object.prototype.hasOwnProperty.call(message, t) && message[t] === value) {
								delete message[t];
								result = true;
							}
						}
					}
				}
			}

			return result;
		};
	});
})(pubsub, pubsub.exports);

var Constants = function Constants() {
	_classCallCheck(this, Constants);
};

_defineProperty(Constants, "LIVE_PREVIEW_UPDATE", "LIVE_PREVIEW_UPDATE");

_defineProperty(Constants, "INITIALIZING_LIVE_PREVIEW", "INITIALIZING_LIVE_PREVIEW");

_defineProperty(Constants, "LIVE_PREVIEW_CHANGE", "LIVE_PREVIEW_CHANGE");

_defineProperty(Constants, "GENERATING_SVG_FROM_SRC", "GENERATING_SVG_FROM_SRC");

_defineProperty(Constants, "RENDERING_LIVE_PREVIEW", "RENDERING_LIVE_PREVIEW");

_defineProperty(Constants, "RENDERING_MONOGRAM_PREVIEW", "RENDERING_MONOGRAM_PREVIEW");

_defineProperty(Constants, "LOADING_SELECTED_VARIANT", "LOADING_SELECTED_VARIANT");

_defineProperty(Constants, "EXTRACTING_CUSTOMIZATIONS_FROM_MONOFIELDS", "EXTRACTING_CUSTOMIZATIONS_FROM_MONOFIELDS");

_defineProperty(Constants, "LOADING_VARIANT_SVG_SOURCE", "LOADING_VARIANT_SVG_SOURCE");

_defineProperty(Constants, "INITIALIZING_MAIN_PRODUCT_IMAGE", "INITIALIZING_MAIN_PRODUCT_IMAGE");

_defineProperty(Constants, "LOADING_TEXTURE_URL", "LOADING_TEXTURE_URL");

_defineProperty(Constants, "APPLYING_CUSTOMIZATION_TO_SVG", "APPLYING_CUSTOMIZATION_TO_SVG");

_defineProperty(Constants, "APPLYING_TEXTURE_TO_SVG", "APPLYING_TEXTURE_TO_SVG");

_defineProperty(Constants, "APPLYING_BACKGROUND_TEXTURE_TO_SVG", "APPLYING_BACKGROUND_TEXTURE_TO_SVG");

_defineProperty(Constants, "APPLYING_DROPSHADOW_TO_SVG", "APPLYING_DROPSHADOW_TO_SVG");

_defineProperty(Constants, "RELOADING_SVG_ELEMENT", "RELOADING_SVG_ELEMENT");

_defineProperty(Constants, "RENDERING_DOWNLOAD_PNG_BUTTON", "RENDERING_DOWNLOAD_PNG_BUTTON");

_defineProperty(Constants, "LOADING_AND_DISPATCHING", "LOADING_AND_DISPATCHING");

_defineProperty(Constants, "LOADING_VARIANT", "LOADING_VARIANT");

_defineProperty(Constants, "LOADING_CUSTOMIZATIONS", "LOADING_CUSTOMIZATIONS");

_defineProperty(Constants, "LOADING_SVG_DATA", "LOADING_SVG_DATA");

var INITIALIZING_LIVE_PREVIEW = function INITIALIZING_LIVE_PREVIEW() {
	return log(Constants.INITIALIZING_LIVE_PREVIEW);
};
var LIVE_PREVIEW_CHANGE = function LIVE_PREVIEW_CHANGE() {
	return log(Constants.LIVE_PREVIEW_CHANGE);
};
var RENDERING_LIVE_PREVIEW = function RENDERING_LIVE_PREVIEW() {
	return log(Constants.RENDERING_LIVE_PREVIEW);
};
var RENDERING_MONOGRAM_PREVIEW = function RENDERING_MONOGRAM_PREVIEW() {
	return log(Constants.RENDERING_MONOGRAM_PREVIEW);
};
var RELOADING_SVG_ELEMENT = function RELOADING_SVG_ELEMENT() {
	return log(Constants.RELOADING_SVG_ELEMENT);
};
var RENDERING_DOWNLOAD_PNG_BUTTON = function RENDERING_DOWNLOAD_PNG_BUTTON() {
	return log(Constants.RENDERING_DOWNLOAD_PNG_BUTTON);
};
var LOADING_AND_DISPATCHING = function LOADING_AND_DISPATCHING() {
	return log(Constants.LOADING_AND_DISPATCHING);
};
var LOADING_VARIANT = function LOADING_VARIANT() {
	return log(Constants.LOADING_VARIANT);
};
var LOADING_CUSTOMIZATIONS = function LOADING_CUSTOMIZATIONS() {
	return log(Constants.LOADING_CUSTOMIZATIONS);
};
var LOADING_SVG_DATA = function LOADING_SVG_DATA() {
	return log(Constants.LOADING_SVG_DATA);
};

// create a function to subscribe to topics
var livePreviewEvents = [];

function initFunction(e) {
	var _event$target, _event$target$id;

	var event = e;
	var variantsMap = monoGramVariants
		.map(function (v) {
			return v.option1 ? v.option1 : "";
		})
		.reduce(function (map, value, i) {
			return (map[value] = true), map;
		}, {});
	event.target.value;
	event.target.name;

	if (
		((_event$target = event.target) !== null &&
			_event$target !== void 0 &&
			(_event$target$id = _event$target.id) !== null &&
			_event$target$id !== void 0 &&
			_event$target$id.toLowerCase().includes("monofield")) ||
		event.path.find(function (p) {
			var _p$getAttribute;

			return (
				p.getAttribute &&
				(((_p$getAttribute = p.getAttribute("id")) === null || _p$getAttribute === void 0
					? void 0
					: _p$getAttribute.toLowerCase().includes("monofield")) ||
					variantsMap[event.target.value])
			);
		})
	) {
		loadAndDispatch();
	}
}

function debounce(func, delay) {
  var timeoutId;
  return function() {
    var context = this,
          args = arguments;
    clearTimeout(timeoutId);
    timeoutId = setTimeout(function() {
      func.apply(context, args);
    }, delay);
  };
}

window.initFunction = debounce(initFunction, 300);

function init() {
	window.addEventListener("input", window.initFunction);
}

function sleep(delay) {
	return new Promise(function (resolve) {
		return setTimeout(resolve, delay);
	});
}
function loadAndDispatch() {
	try {
		sleep(20)
			.then(function () {
				LOADING_AND_DISPATCHING();
				var payload = loadData();
				log(payload);

				if (livePreviewEvents[livePreviewEvents.length - 1] !== payload) {
					pubsub.exports.publish(Constants.LIVE_PREVIEW_UPDATE, payload);
				}
			})
			["catch"](log);
	} catch (error) {
		console.error(error);
	}
}
function loadVariant() {
	LOADING_VARIANT();
	var variantSelector = document.querySelector("[name^=id]");
	var variantId = variantSelector === null || variantSelector === void 0 ? void 0 : variantSelector.value;
	var variant = monoGramVariants.find(function (v) {
		return "".concat(v.id) == variantId;
	});
	return variant || {};
}
function loadPersonalizations() {
	LOADING_CUSTOMIZATIONS();
	var selectFields = document.querySelectorAll("select[id^=monofield]");
	var selects = Array.from(selectFields).map(function (sv) {
		var extractedProperty = sv.name.replace("properties[", "").replace("]", "");
		var value = sv.value.replace(" ", "_");
		var position = extractedProperty ? extractedProperty.replace(" ", "_") : "";
		return {
			value: value,
			position: position,
		};
	});
	var textFields = document.querySelectorAll("input[id^=monofield]");
	var texts = Array.from(textFields).map(function (t) {
		var value = t.value.toUpperCase();
		var extractedProp = t.name.replace("properties[", "").replace("]", "");
		log(extractedProp);
		var position = extractedProp.length ? extractedProp.replace(" ", "_") : "";

		if (!position.length) {
			error("Could not find position for ".concat(t.id, ". This customization text won't be applied."));
		}

		if (position.includes("Name") && selects.length > 0) {
			position = "".concat(selects[0].value, "_").concat(selects[0].position, "-").concat(position);
		}

		log({
			value: value,
			position: position,
		});
		return {
			value: value,
			position: position,
		};
	});
	var event = {
		selectMap: selects.reduce(function (map, value, i) {
			return (map["".concat(value.position)] = value.value), map;
		}, {}),
		textMap: texts.reduce(function (map, value, i) {
			return (map["".concat(value.position)] = value.value), map;
		}, {}),
	};
	return event;
}
function loadData() {
	LOADING_SVG_DATA();
	var payload = {
		customizations: loadPersonalizations(),
		variant: loadVariant(),
	};
	var event = {
		option1: payload.variant.option1 || "",
		option2: payload.variant.option2 || "",
		option3: payload.variant.option3 || "",
		customizations: payload.customizations,
		backgroundImageSrc: monoGramData.backgroundImageSrc,
	};
	return event;
}

function generateSvgElement(tag) {
	return document.createElementNS("http://www.w3.org/2000/svg", tag);
}
function getTextureUrl(color) {
	return "https://prodmyeasymonogram.s3.us-east-2.amazonaws.com/Product/03+-+Preview+Image+Textures/".concat(color, ".png");
}
function loadTextureUrl(event) {
	// Get the options available to the variant
	// Index into the options array to get the color (should be the first entry)
	var color = event.option1 || "";
	var textureUrl = getTextureUrl(color);
	return textureUrl;
}
function reloadSvgElement(svg) {
	var _svg$parentNode;

	RELOADING_SVG_ELEMENT();
	var div = document.createElement("div");
	div.appendChild(svg.cloneNode(true));
	var dom = new DOMParser().parseFromString(div.innerHTML, "application/xml");
	svg === null || svg === void 0
		? void 0
		: (_svg$parentNode = svg.parentNode) === null || _svg$parentNode === void 0
		? void 0
		: _svg$parentNode.replaceChild(document.importNode(dom.documentElement, true), svg);
}

function loadBaseSvgContent(_x2) {
	return _loadBaseSvgContent.apply(this, arguments);
}
/**
 * Given an SVG string, generate and return an SVGSVGElement.
 * @param baseSvgContent The base svg content in serialized form
 * @returns An SVGElement node
 */

function _loadBaseSvgContent() {
	_loadBaseSvgContent = _asyncToGenerator(
		/*#__PURE__*/ regeneratorRuntime.mark(function _callee2(baseSvgUrl) {
			var baseSvgContent, node;
			return regeneratorRuntime.wrap(function _callee2$(_context2) {
				while (1) {
					switch ((_context2.prev = _context2.next)) {
						case 0:
							baseSvgContent = "";

							if (baseSvgContent) {
								_context2.next = 7;
								break;
							}

							_context2.next = 4;
							return fetch(baseSvgUrl, {
								method: "GET",
								mode: "cors",
								cache: "no-cache",
								headers: {
									Origin: window.location.origin,
								},
							}).then(function (resp) {
								return resp.text();
							});

						case 4:
							baseSvgContent = _context2.sent;
							node = convertSvgStrToElement(baseSvgContent);
							baseSvgContent = new XMLSerializer().serializeToString(node);

						case 7:
							return _context2.abrupt("return", baseSvgContent);

						case 8:
						case "end":
							return _context2.stop();
					}
				}
			}, _callee2);
		})
	);
	return _loadBaseSvgContent.apply(this, arguments);
}

function convertSvgStrToElement(baseSvgContent) {
	// Create a document parser, so we can read the SVG content
	var parser = new DOMParser(); // Find the relevant data from the SVG content

	var xmlDoc = parser.parseFromString(baseSvgContent, "image/svg+xml"); // Get the SVG element, so nothing else from the response content is included

	var svgElement = xmlDoc.querySelector("svg");

	if (svgElement) {
		// Fix for Brad sticking a <div> in his <style>
		var innerStyle = svgElement.querySelector("style");
		var innerStyleDiv = svgElement.querySelector("style > div");

		if (innerStyle && innerStyleDiv) {
			var styleDivText = innerStyleDiv.childNodes[0];
			innerStyle.append(styleDivText);
			innerStyleDiv.remove();
		}
	}

	return svgElement;
}

function applyTextureToSvg(svg, textureUrl) {
	var patternDiv = build_pattern_element(textureUrl);

	if (patternDiv) {
		var _svg$querySelector;

		(_svg$querySelector = svg.querySelector("style")) === null || _svg$querySelector === void 0
			? void 0
			: _svg$querySelector.insertAdjacentElement("afterend", patternDiv);
		svg.querySelectorAll("g").forEach(function (t) {
			t.style.fill = "url(#pattern)";
		});
	}

	return svg;
}
function build_pattern_element(textureUrl) {
	var patternDiv = document.createElementNS("http://www.w3.org/2000/svg", "pattern");
	patternDiv.setAttributeNS("http://www.w3.org/2000/svg", "id", "pattern");
	patternDiv.setAttributeNS("http://www.w3.org/2000/svg", "patternUnits", "userSpaceOnUse");
	patternDiv.setAttribute("width", "1000");
	patternDiv.setAttribute("height", "1000");
	var imgDiv = document.createElement("image");
	imgDiv.setAttribute("width", "1000");
	imgDiv.setAttribute("height", "1000");
	imgDiv.setAttributeNS("http://www.w3.org/2000/svg", "href", textureUrl);
	var patternGroup = document.createElementNS("http://www.w3.org/2000/svg", "g");
	patternGroup.setAttribute("id", "Colors");
	patternGroup.classList.add("st0");
	patternDiv.appendChild(imgDiv);
	patternGroup.appendChild(patternDiv);
	return patternGroup;
}

function applyBackground(svg, textureUrl) {
	var default_background_src = "https://prodmyeasymonogram.s3.us-east-2.amazonaws.com/Product/05+-+Live+Preview+Backgrounds/BG1.png";
	if (textureUrl) {
		svg.style.backgroundImage = "url(".concat(textureUrl, ")");
	} else {
		svg.style.backgroundImage = "url(".concat(default_background_src, ")");
	}
	svg.style.backgroundSize = "100%";
	svg.style.backgroundRepeat = 'no-repeat';
	return svg;
}

function applyFlatten(svg) {
	var _designNode$querySele;

	var textGroups = Array.from(svg.querySelectorAll("g.text"));
	var childNodes = textGroups.flatMap(function (g) {
		return Array.from(g.childNodes);
	});
	var designNode = svg.querySelector("[id^=Design]");
	var designParent =
		designNode === null || designNode === void 0
			? void 0
			: (_designNode$querySele = designNode.querySelector("path")) === null || _designNode$querySele === void 0
			? void 0
			: _designNode$querySele.parentNode;
	var designPath = designParent === null || designParent === void 0 ? void 0 : designParent.firstChild;

	if (designParent && designPath) {
		childNodes.forEach(function (c) {
			if (designPath != undefined) designParent === null || designParent === void 0 ? void 0 : designParent.append(c, designPath);
		});
	}

	return svg;
}

function applyDropshadowToSvg(svg) {
	var _svg$querySelector;

	var defs = generateSvgElement("defs");
	var dsFilter = generateSvgElement("filter");
	var feDs = generateSvgElement("feDropShadow");
	var bottomlessDsFilter = generateSvgElement("filter");
	var feBottomDs = generateSvgElement("feDropShadow");
	dsFilter.setAttribute("id", "dropshadow");
	feDs.setAttribute("dx", "2");
	feDs.setAttribute("dy", "2");
	feDs.setAttribute("stdDeviation", "3");
	dsFilter.appendChild(feDs);
	bottomlessDsFilter.setAttribute("id", "bottomless_dropshadow");
	feBottomDs.setAttribute("dx", "2");
	feBottomDs.setAttribute("dy", "-3");
	feBottomDs.setAttribute("stdDeviation", "1");
	bottomlessDsFilter.appendChild(feBottomDs);
	defs.appendChild(dsFilter);
	defs.appendChild(bottomlessDsFilter);
	(_svg$querySelector = svg.querySelector("style")) === null || _svg$querySelector === void 0
		? void 0
		: _svg$querySelector.insertAdjacentElement("afterend", defs);
	svg.querySelectorAll("g[id]:not(.text):not(.variant)").forEach(function (t) {
		t.style.filter = "url(#dropshadow)";
	});
	svg.querySelectorAll("g[id].text, g[id].variant").forEach(function (t) {
		t.querySelectorAll("g[id]").forEach(function (e) {
			e.style.filter = "url(#bottomless_dropshadow)";
		});
	});
	return svg;
}

/**
 * Given an SVG Element and a LivePreviewEvent, apply customizations and modifications and return the modified SVGElement
 * @param svg SVGElement to be modified
 * @param event LivePreviewEvent to be applied to SVGElement
 */

function applyChangesToSvgElement(svg, event) {
	var customTextGroups = svg.querySelectorAll("[id^=Custom_Text]");
	customTextGroups.forEach(function (n) {
		return svg.append(n);
	});
	svg = applyPersonalizationToSvg(svg, event.customizations);
	var textureUrl = loadTextureUrl(event);
	svg = applyTextureToSvg(svg, textureUrl);
	svg = applyBackground(svg, event.backgroundImageSrc); // Flatten SVG by extracting path and text elements from custom text groups and appending them to the design group

	svg = applyFlatten(svg);
	svg = applyDropshadowToSvg(svg);
	return svg;
}

function svg2img(_x) {
	return _svg2img.apply(this, arguments);
}

function _svg2img() {
	_svg2img = _asyncToGenerator(
		/*#__PURE__*/ regeneratorRuntime.mark(function _callee(name) {
			var document_name, svg, productSvg, designGroup, originalDesignFill, xml, svg64, b64start, image64, width, resp;
			return regeneratorRuntime.wrap(function _callee$(_context) {
				while (1) {
					switch ((_context.prev = _context.next)) {
						case 0:
							document_name = name.replace(/([\/\"-])/g, "").replace(/\\s/g, "_");
							svg = document.querySelector("#mem-canvas-container > svg");

							if (!svg) {
								_context.next = 18;
								break;
							}

							productSvg = document.querySelector('#mem-canvas-container > svg > g[id="Product"] > svg');
							designGroup = svg.querySelector("g#Design");

							if (productSvg) {
								designGroup = productSvg.querySelector("g#Design");
							}

							originalDesignFill = "";

							if (designGroup) {
								originalDesignFill = designGroup.style.fill;
								designGroup.style.fill = "#000";
							}

							xml = new XMLSerializer().serializeToString(svg);
							svg64 = btoa(xml); //for utf8: btoa(unescape(encodeURIComponent(xml)))

							b64start = "data:image/svg+xml;base64,";
							image64 = b64start + svg64;
							width = 2000;
							_context.next = 15;
							return new Promise(function (resolve) {
								var img = document.createElement("img");

								img.onload = function () {
									document.body.appendChild(img);
									var canvas = document.createElement("canvas");
									var ratio = img.clientWidth / img.clientHeight || 1;
									document.body.removeChild(img);
									canvas.width = width;
									canvas.height = width / ratio;
									var ctx = canvas.getContext("2d");

									if (ctx) {
										ctx.drawImage(img, 0, 0, canvas.width, canvas.height);

										try {
											var data = canvas.toDataURL("image/png");
											resolve(data);
										} catch (e) {
											resolve("error " + e);
										}
									}
								};

								img.src = image64;
							});

						case 15:
							resp = _context.sent;
							download(resp, document_name);

							if (designGroup) {
								designGroup.style.fill = originalDesignFill;
							}

						case 18:
						case "end":
							return _context.stop();
					}
				}
			}, _callee);
		})
	);
	return _svg2img.apply(this, arguments);
}

var download = function download(href, name) {
	var link = document.createElement("a");
	link.download = name;
	link.style.opacity = "0";
	document.body.append(link);
	link.href = href;
	link.click();
	link.remove();
};

var runtime = { exports: {} };

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

(function (module) {
	var runtime = (function (exports) {
		var Op = Object.prototype;
		var hasOwn = Op.hasOwnProperty;
		var undefined$1; // More compressible than void 0.
		var $Symbol = typeof Symbol === "function" ? Symbol : {};
		var iteratorSymbol = $Symbol.iterator || "@@iterator";
		var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
		var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

		function define(obj, key, value) {
			Object.defineProperty(obj, key, {
				value: value,
				enumerable: true,
				configurable: true,
				writable: true,
			});
			return obj[key];
		}
		try {
			// IE 8 has a broken Object.defineProperty that only works on DOM objects.
			define({}, "");
		} catch (err) {
			define = function (obj, key, value) {
				return (obj[key] = value);
			};
		}

		function wrap(innerFn, outerFn, self, tryLocsList) {
			// If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
			var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
			var generator = Object.create(protoGenerator.prototype);
			var context = new Context(tryLocsList || []);

			// The ._invoke method unifies the implementations of the .next,
			// .throw, and .return methods.
			generator._invoke = makeInvokeMethod(innerFn, self, context);

			return generator;
		}
		exports.wrap = wrap;

		// Try/catch helper to minimize deoptimizations. Returns a completion
		// record like context.tryEntries[i].completion. This interface could
		// have been (and was previously) designed to take a closure to be
		// invoked without arguments, but in all the cases we care about we
		// already have an existing method we want to call, so there's no need
		// to create a new function object. We can even get away with assuming
		// the method takes exactly one argument, since that happens to be true
		// in every case, so we don't have to touch the arguments object. The
		// only additional allocation required is the completion record, which
		// has a stable shape and so hopefully should be cheap to allocate.
		function tryCatch(fn, obj, arg) {
			try {
				return { type: "normal", arg: fn.call(obj, arg) };
			} catch (err) {
				return { type: "throw", arg: err };
			}
		}

		var GenStateSuspendedStart = "suspendedStart";
		var GenStateSuspendedYield = "suspendedYield";
		var GenStateExecuting = "executing";
		var GenStateCompleted = "completed";

		// Returning this object from the innerFn has the same effect as
		// breaking out of the dispatch switch statement.
		var ContinueSentinel = {};

		// Dummy constructor functions that we use as the .constructor and
		// .constructor.prototype properties for functions that return Generator
		// objects. For full spec compliance, you may wish to configure your
		// minifier not to mangle the names of these two functions.
		function Generator() {}
		function GeneratorFunction() {}
		function GeneratorFunctionPrototype() {}

		// This is a polyfill for %IteratorPrototype% for environments that
		// don't natively support it.
		var IteratorPrototype = {};
		define(IteratorPrototype, iteratorSymbol, function () {
			return this;
		});

		var getProto = Object.getPrototypeOf;
		var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
		if (NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
			// This environment has a native %IteratorPrototype%; use it instead
			// of the polyfill.
			IteratorPrototype = NativeIteratorPrototype;
		}

		var Gp = (GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype));
		GeneratorFunction.prototype = GeneratorFunctionPrototype;
		define(Gp, "constructor", GeneratorFunctionPrototype);
		define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
		GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction");

		// Helper for defining the .next, .throw, and .return methods of the
		// Iterator interface in terms of a single ._invoke method.
		function defineIteratorMethods(prototype) {
			["next", "throw", "return"].forEach(function (method) {
				define(prototype, method, function (arg) {
					return this._invoke(method, arg);
				});
			});
		}

		exports.isGeneratorFunction = function (genFun) {
			var ctor = typeof genFun === "function" && genFun.constructor;
			return ctor
				? ctor === GeneratorFunction ||
						// For the native GeneratorFunction constructor, the best we can
						// do is to check its .name property.
						(ctor.displayName || ctor.name) === "GeneratorFunction"
				: false;
		};

		exports.mark = function (genFun) {
			if (Object.setPrototypeOf) {
				Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
			} else {
				genFun.__proto__ = GeneratorFunctionPrototype;
				define(genFun, toStringTagSymbol, "GeneratorFunction");
			}
			genFun.prototype = Object.create(Gp);
			return genFun;
		};

		// Within the body of any async function, "await x" is transformed to
		// "yield regeneratorRuntime.awrap(x)", so that the runtime can test
		// "hasOwn.call(value, "__await")" to determine if the yielded value is
		// meant to be awaited.
		exports.awrap = function (arg) {
			return { __await: arg };
		};

		function AsyncIterator(generator, PromiseImpl) {
			function invoke(method, arg, resolve, reject) {
				var record = tryCatch(generator[method], generator, arg);
				if (record.type === "throw") {
					reject(record.arg);
				} else {
					var result = record.arg;
					var value = result.value;
					if (value && typeof value === "object" && hasOwn.call(value, "__await")) {
						return PromiseImpl.resolve(value.__await).then(
							function (value) {
								invoke("next", value, resolve, reject);
							},
							function (err) {
								invoke("throw", err, resolve, reject);
							}
						);
					}

					return PromiseImpl.resolve(value).then(
						function (unwrapped) {
							// When a yielded Promise is resolved, its final value becomes
							// the .value of the Promise<{value,done}> result for the
							// current iteration.
							result.value = unwrapped;
							resolve(result);
						},
						function (error) {
							// If a rejected Promise was yielded, throw the rejection back
							// into the async generator function so it can be handled there.
							return invoke("throw", error, resolve, reject);
						}
					);
				}
			}

			var previousPromise;

			function enqueue(method, arg) {
				function callInvokeWithMethodAndArg() {
					return new PromiseImpl(function (resolve, reject) {
						invoke(method, arg, resolve, reject);
					});
				}

				return (previousPromise =
					// If enqueue has been called before, then we want to wait until
					// all previous Promises have been resolved before calling invoke,
					// so that results are always delivered in the correct order. If
					// enqueue has not been called before, then it is important to
					// call invoke immediately, without waiting on a callback to fire,
					// so that the async generator function has the opportunity to do
					// any necessary setup in a predictable way. This predictability
					// is why the Promise constructor synchronously invokes its
					// executor callback, and why async functions synchronously
					// execute code before the first await. Since we implement simple
					// async functions in terms of async generators, it is especially
					// important to get this right, even though it requires care.
					previousPromise
						? previousPromise.then(
								callInvokeWithMethodAndArg,
								// Avoid propagating failures to Promises returned by later
								// invocations of the iterator.
								callInvokeWithMethodAndArg
						  )
						: callInvokeWithMethodAndArg());
			}

			// Define the unified helper method that is used to implement .next,
			// .throw, and .return (see defineIteratorMethods).
			this._invoke = enqueue;
		}

		defineIteratorMethods(AsyncIterator.prototype);
		define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
			return this;
		});
		exports.AsyncIterator = AsyncIterator;

		// Note that simple async functions are implemented on top of
		// AsyncIterator objects; they just return a Promise for the value of
		// the final result produced by the iterator.
		exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
			if (PromiseImpl === void 0) PromiseImpl = Promise;

			var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);

			return exports.isGeneratorFunction(outerFn)
				? iter // If outerFn is a generator, return the full iterator.
				: iter.next().then(function (result) {
						return result.done ? result.value : iter.next();
				  });
		};

		function makeInvokeMethod(innerFn, self, context) {
			var state = GenStateSuspendedStart;

			return function invoke(method, arg) {
				if (state === GenStateExecuting) {
					throw new Error("Generator is already running");
				}

				if (state === GenStateCompleted) {
					if (method === "throw") {
						throw arg;
					}

					// Be forgiving, per 25.3.3.3.3 of the spec:
					// https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
					return doneResult();
				}

				context.method = method;
				context.arg = arg;

				while (true) {
					var delegate = context.delegate;
					if (delegate) {
						var delegateResult = maybeInvokeDelegate(delegate, context);
						if (delegateResult) {
							if (delegateResult === ContinueSentinel) continue;
							return delegateResult;
						}
					}

					if (context.method === "next") {
						// Setting context._sent for legacy support of Babel's
						// function.sent implementation.
						context.sent = context._sent = context.arg;
					} else if (context.method === "throw") {
						if (state === GenStateSuspendedStart) {
							state = GenStateCompleted;
							throw context.arg;
						}

						context.dispatchException(context.arg);
					} else if (context.method === "return") {
						context.abrupt("return", context.arg);
					}

					state = GenStateExecuting;

					var record = tryCatch(innerFn, self, context);
					if (record.type === "normal") {
						// If an exception is thrown from innerFn, we leave state ===
						// GenStateExecuting and loop back for another invocation.
						state = context.done ? GenStateCompleted : GenStateSuspendedYield;

						if (record.arg === ContinueSentinel) {
							continue;
						}

						return {
							value: record.arg,
							done: context.done,
						};
					} else if (record.type === "throw") {
						state = GenStateCompleted;
						// Dispatch the exception by looping back around to the
						// context.dispatchException(context.arg) call above.
						context.method = "throw";
						context.arg = record.arg;
					}
				}
			};
		}

		// Call delegate.iterator[context.method](context.arg) and handle the
		// result, either by returning a { value, done } result from the
		// delegate iterator, or by modifying context.method and context.arg,
		// setting context.delegate to null, and returning the ContinueSentinel.
		function maybeInvokeDelegate(delegate, context) {
			var method = delegate.iterator[context.method];
			if (method === undefined$1) {
				// A .throw or .return when the delegate iterator has no .throw
				// method always terminates the yield* loop.
				context.delegate = null;

				if (context.method === "throw") {
					// Note: ["return"] must be used for ES3 parsing compatibility.
					if (delegate.iterator["return"]) {
						// If the delegate iterator has a return method, give it a
						// chance to clean up.
						context.method = "return";
						context.arg = undefined$1;
						maybeInvokeDelegate(delegate, context);

						if (context.method === "throw") {
							// If maybeInvokeDelegate(context) changed context.method from
							// "return" to "throw", let that override the TypeError below.
							return ContinueSentinel;
						}
					}

					context.method = "throw";
					context.arg = new TypeError("The iterator does not provide a 'throw' method");
				}

				return ContinueSentinel;
			}

			var record = tryCatch(method, delegate.iterator, context.arg);

			if (record.type === "throw") {
				context.method = "throw";
				context.arg = record.arg;
				context.delegate = null;
				return ContinueSentinel;
			}

			var info = record.arg;

			if (!info) {
				context.method = "throw";
				context.arg = new TypeError("iterator result is not an object");
				context.delegate = null;
				return ContinueSentinel;
			}

			if (info.done) {
				// Assign the result of the finished delegate to the temporary
				// variable specified by delegate.resultName (see delegateYield).
				context[delegate.resultName] = info.value;

				// Resume execution at the desired location (see delegateYield).
				context.next = delegate.nextLoc;

				// If context.method was "throw" but the delegate handled the
				// exception, let the outer generator proceed normally. If
				// context.method was "next", forget context.arg since it has been
				// "consumed" by the delegate iterator. If context.method was
				// "return", allow the original .return call to continue in the
				// outer generator.
				if (context.method !== "return") {
					context.method = "next";
					context.arg = undefined$1;
				}
			} else {
				// Re-yield the result returned by the delegate method.
				return info;
			}

			// The delegate iterator is finished, so forget it and continue with
			// the outer generator.
			context.delegate = null;
			return ContinueSentinel;
		}

		// Define Generator.prototype.{next,throw,return} in terms of the
		// unified ._invoke helper method.
		defineIteratorMethods(Gp);

		define(Gp, toStringTagSymbol, "Generator");

		// A Generator should always return itself as the iterator object when the
		// @@iterator function is called on it. Some browsers' implementations of the
		// iterator prototype chain incorrectly implement this, causing the Generator
		// object to not be returned from this call. This ensures that doesn't happen.
		// See https://github.com/facebook/regenerator/issues/274 for more details.
		define(Gp, iteratorSymbol, function () {
			return this;
		});

		define(Gp, "toString", function () {
			return "[object Generator]";
		});

		function pushTryEntry(locs) {
			var entry = { tryLoc: locs[0] };

			if (1 in locs) {
				entry.catchLoc = locs[1];
			}

			if (2 in locs) {
				entry.finallyLoc = locs[2];
				entry.afterLoc = locs[3];
			}

			this.tryEntries.push(entry);
		}

		function resetTryEntry(entry) {
			var record = entry.completion || {};
			record.type = "normal";
			delete record.arg;
			entry.completion = record;
		}

		function Context(tryLocsList) {
			// The root entry object (effectively a try statement without a catch
			// or a finally block) gives us a place to store values thrown from
			// locations where there is no enclosing try statement.
			this.tryEntries = [{ tryLoc: "root" }];
			tryLocsList.forEach(pushTryEntry, this);
			this.reset(true);
		}

		exports.keys = function (object) {
			var keys = [];
			for (var key in object) {
				keys.push(key);
			}
			keys.reverse();

			// Rather than returning an object with a next method, we keep
			// things simple and return the next function itself.
			return function next() {
				while (keys.length) {
					var key = keys.pop();
					if (key in object) {
						next.value = key;
						next.done = false;
						return next;
					}
				}

				// To avoid creating an additional object, we just hang the .value
				// and .done properties off the next function object itself. This
				// also ensures that the minifier will not anonymize the function.
				next.done = true;
				return next;
			};
		};

		function values(iterable) {
			if (iterable) {
				var iteratorMethod = iterable[iteratorSymbol];
				if (iteratorMethod) {
					return iteratorMethod.call(iterable);
				}

				if (typeof iterable.next === "function") {
					return iterable;
				}

				if (!isNaN(iterable.length)) {
					var i = -1,
						next = function next() {
							while (++i < iterable.length) {
								if (hasOwn.call(iterable, i)) {
									next.value = iterable[i];
									next.done = false;
									return next;
								}
							}

							next.value = undefined$1;
							next.done = true;

							return next;
						};

					return (next.next = next);
				}
			}

			// Return an iterator with no values.
			return { next: doneResult };
		}
		exports.values = values;

		function doneResult() {
			return { value: undefined$1, done: true };
		}

		Context.prototype = {
			constructor: Context,

			reset: function (skipTempReset) {
				this.prev = 0;
				this.next = 0;
				// Resetting context._sent for legacy support of Babel's
				// function.sent implementation.
				this.sent = this._sent = undefined$1;
				this.done = false;
				this.delegate = null;

				this.method = "next";
				this.arg = undefined$1;

				this.tryEntries.forEach(resetTryEntry);

				if (!skipTempReset) {
					for (var name in this) {
						// Not sure about the optimal order of these conditions:
						if (name.charAt(0) === "t" && hasOwn.call(this, name) && !isNaN(+name.slice(1))) {
							this[name] = undefined$1;
						}
					}
				}
			},

			stop: function () {
				this.done = true;

				var rootEntry = this.tryEntries[0];
				var rootRecord = rootEntry.completion;
				if (rootRecord.type === "throw") {
					throw rootRecord.arg;
				}

				return this.rval;
			},

			dispatchException: function (exception) {
				if (this.done) {
					throw exception;
				}

				var context = this;
				function handle(loc, caught) {
					record.type = "throw";
					record.arg = exception;
					context.next = loc;

					if (caught) {
						// If the dispatched exception was caught by a catch block,
						// then let that catch block handle the exception normally.
						context.method = "next";
						context.arg = undefined$1;
					}

					return !!caught;
				}

				for (var i = this.tryEntries.length - 1; i >= 0; --i) {
					var entry = this.tryEntries[i];
					var record = entry.completion;

					if (entry.tryLoc === "root") {
						// Exception thrown outside of any try block that could handle
						// it, so set the completion value of the entire function to
						// throw the exception.
						return handle("end");
					}

					if (entry.tryLoc <= this.prev) {
						var hasCatch = hasOwn.call(entry, "catchLoc");
						var hasFinally = hasOwn.call(entry, "finallyLoc");

						if (hasCatch && hasFinally) {
							if (this.prev < entry.catchLoc) {
								return handle(entry.catchLoc, true);
							} else if (this.prev < entry.finallyLoc) {
								return handle(entry.finallyLoc);
							}
						} else if (hasCatch) {
							if (this.prev < entry.catchLoc) {
								return handle(entry.catchLoc, true);
							}
						} else if (hasFinally) {
							if (this.prev < entry.finallyLoc) {
								return handle(entry.finallyLoc);
							}
						} else {
							throw new Error("try statement without catch or finally");
						}
					}
				}
			},

			abrupt: function (type, arg) {
				for (var i = this.tryEntries.length - 1; i >= 0; --i) {
					var entry = this.tryEntries[i];
					if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
						var finallyEntry = entry;
						break;
					}
				}

				if (finallyEntry && (type === "break" || type === "continue") && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc) {
					// Ignore the finally entry if control is not jumping to a
					// location outside the try/catch block.
					finallyEntry = null;
				}

				var record = finallyEntry ? finallyEntry.completion : {};
				record.type = type;
				record.arg = arg;

				if (finallyEntry) {
					this.method = "next";
					this.next = finallyEntry.finallyLoc;
					return ContinueSentinel;
				}

				return this.complete(record);
			},

			complete: function (record, afterLoc) {
				if (record.type === "throw") {
					throw record.arg;
				}

				if (record.type === "break" || record.type === "continue") {
					this.next = record.arg;
				} else if (record.type === "return") {
					this.rval = this.arg = record.arg;
					this.method = "return";
					this.next = "end";
				} else if (record.type === "normal" && afterLoc) {
					this.next = afterLoc;
				}

				return ContinueSentinel;
			},

			finish: function (finallyLoc) {
				for (var i = this.tryEntries.length - 1; i >= 0; --i) {
					var entry = this.tryEntries[i];
					if (entry.finallyLoc === finallyLoc) {
						this.complete(entry.completion, entry.afterLoc);
						resetTryEntry(entry);
						return ContinueSentinel;
					}
				}
			},

			catch: function (tryLoc) {
				for (var i = this.tryEntries.length - 1; i >= 0; --i) {
					var entry = this.tryEntries[i];
					if (entry.tryLoc === tryLoc) {
						var record = entry.completion;
						if (record.type === "throw") {
							var thrown = record.arg;
							resetTryEntry(entry);
						}
						return thrown;
					}
				}

				// The context.catch method must only be called with a location
				// argument that corresponds to a known catch block.
				throw new Error("illegal catch attempt");
			},

			delegateYield: function (iterable, resultName, nextLoc) {
				this.delegate = {
					iterator: values(iterable),
					resultName: resultName,
					nextLoc: nextLoc,
				};

				if (this.method === "next") {
					// Deliberately forget the last sent value so that we don't
					// accidentally pass it on to the delegate.
					this.arg = undefined$1;
				}

				return ContinueSentinel;
			},
		};

		// Regardless of whether this script is executing as a CommonJS module
		// or not, return the runtime object so that we can declare the variable
		// regeneratorRuntime in the outer scope, which allows this module to be
		// injected easily by "bin/regenerator --include-runtime script.js".
		return exports;
	})(
		// If this script is executing as a CommonJS module, use module.exports
		// as the regeneratorRuntime namespace. Otherwise create a new empty
		// object. Either way, the resulting object will be used to initialize
		// the regeneratorRuntime variable at the top of this file.
		module.exports
	);

	try {
		regeneratorRuntime = runtime;
	} catch (accidentalStrictMode) {
		// This module should not be running in strict mode, so the above
		// assignment should always work unless something is misconfigured. Just
		// in case runtime.js accidentally runs in strict mode, in modern engines
		// we can explicitly access globalThis. In older engines we can escape
		// strict mode using a global Function call. This could conceivably fail
		// if a Content Security Policy forbids using Function, but in that case
		// the proper solution is to fix the accidental strict mode problem. If
		// you've misconfigured your bundler to force strict mode and applied a
		// CSP to forbid Function, and you're not willing to fix either of those
		// problems, please detail your unique predicament in a GitHub issue.
		if (typeof globalThis === "object") {
			globalThis.regeneratorRuntime = runtime;
		} else {
			Function("r", "regeneratorRuntime = r")(runtime);
		}
	}
})(runtime);

initializeLivePreview();
/**
 * Initialize the live preview in a Shopify product page by
 * linking the AddToCart form inputs to the LivePreviewEvent publisher
 */

function initializeLivePreview() {
	var _document$getElementB;

	(_document$getElementB = document.getElementById("monoPreview")) === null || _document$getElementB === void 0
		? void 0
		: _document$getElementB.addEventListener("click", function () {
				if (isMobile()) window.scrollTo(0, 0);
		  });
	INITIALIZING_LIVE_PREVIEW();
	initializeNamesInputs();
	pubsub.exports.subscribe(Constants.LIVE_PREVIEW_UPDATE, onLivePreviewChange);
	init();
	renderMonogramPreview();
	if (isMobile()) window.scrollTo(0, 0);
}
/**
 * LivePreviewEvent change handler; loads SVG and applies updated customization payload to SVG
 * @param topic The LivePreviewEvent topic
 * @param data The LivePreviewEvent payload
 */

function onLivePreviewChange(topic, data) {
	LIVE_PREVIEW_CHANGE();
	var svgSrc = loadVariantSvgSource();
	var bgSrc = loadBackgroundSvgSource(); // Load base svg content, convert to SVGElement, apply customizations

	loadBaseSvgContent(svgSrc).then(function (prodSvgContent) {
		if (bgSrc !== null && bgSrc !== void 0 && bgSrc.includes("svg")) {
			loadBaseSvgContent(bgSrc).then(function (backgroundSvgContent) {
				renderLivePreview(prodSvgContent, data, backgroundSvgContent);
			});
		} else {
			renderLivePreview(prodSvgContent, data);
		}
	});
}
/**
 * Loads the selected variant, customizations, and SVG and applies customizations to the SVG
 */

function renderMonogramPreview() {
	RENDERING_MONOGRAM_PREVIEW();
	var selectedVariant = monogramSelectedVariant();
	var customizations = extractCustomizationsFromMonofields();
	var data = {
		option1: selectedVariant.option1 || "",
		option2: selectedVariant.option2 || "",
		option3: selectedVariant.option3 || "",
		customizations: customizations,
		backgroundImageSrc: monoGramData.backgroundImageSrc,
	};
	var svgSrc = loadVariantSvgSource();
	var bgSrc = loadBackgroundSvgSource();
	loadBaseSvgContent(svgSrc).then(function (prodSvgContent) {
		if (bgSrc !== null && bgSrc !== void 0 && bgSrc.includes("svg")) {
			loadBaseSvgContent(bgSrc).then(function (backgroundSvgContent) {
				renderLivePreview(prodSvgContent, data, backgroundSvgContent);
			});
		} else {
			renderLivePreview(prodSvgContent, data);
		}
	});
}

function extractCustomizationsFromMonofields() {
	var event = loadPersonalizations();
	return event;
}
/**
 * Loads and injects a live preview image container into the main product image
 * Generates a new SVG element from applying customization changes to base SVG content
 * Appends the new SVG element to the main product image container
 * @param productSvgContent The base svg content string, used to build the SVG Element
 * @param event LivePreviewEvent payload, used to apply customizations to the SVG
 */

function renderLivePreview(productSvgContent, event, backgroundSvgContent) {
	RENDERING_LIVE_PREVIEW();

	if (productSvgContent && event) {
		var productImageContainer = injectLivePreviewImageIntoMainProductImageContainer();

		if (productImageContainer) {
			productImageContainer.innerText = "";
			var svgElement = convertSvgStrToElement(productSvgContent);

			/*
				MANUALLY MAKING IT FALSE AS IT IS CONFLICTS WHILE
				INSERTING BG IMAGE FROM DASHBOARD AND ELSE IS WORKING
				FINE WITH BG ELEMENT & WITHOUT IT (condition: backgroundSvgContent)
			*/
			if (false) {
				var bgElement = convertSvgStrToElement(backgroundSvgContent);
				var productNode = bgElement.querySelector('g[id="Product"]');

				if (productNode) {
					var rectNode = productNode.querySelector("rect"); // Copy rect element attributes to SVG

					["x", "y", "width", "height"].forEach(function (attr) {
						if (rectNode) {
							svgElement.setAttribute(attr, rectNode.getAttribute(attr) || "");
						}
					});
					productNode === null || productNode === void 0 ? void 0 : productNode.append(svgElement); // rectNode?.remove();
				}

				productImageContainer.append(bgElement);
				svgElement = applyChangesToSvgElement(svgElement, event);

				if (isMobile()) {
					svgElement.setAttribute("height", "100%");
					svgElement.setAttribute("width", "100%");
				}

				reloadSvgElement(svgElement);
				renderDownloadPngBtn(productImageContainer, event);
			} else {
				productImageContainer.append(svgElement);
				svgElement = applyChangesToSvgElement(svgElement, event);
				svgElement.setAttribute("height", "100%");
				svgElement.setAttribute("width", "100%");
				reloadSvgElement(svgElement);
				renderDownloadPngBtn(productImageContainer, event);
			}
		}
	}
}

function renderDownloadPngBtn(productImageContainer, selectedVariant) {
	RENDERING_DOWNLOAD_PNG_BUTTON();
	var isAdmin = document.querySelectorAll("iframe[id^=admin-bar-iframe]").length;

	if (isAdmin) {
		var svg2img_button = document.createElement("button");
		svg2img_button.id = "svg2img-button";
		svg2img_button.style.margin = "1rem";
		svg2img_button.style.display = "inline-block";
		svg2img_button.innerHTML = "Download PNG";
		productImageContainer.appendChild(svg2img_button);
		productImageContainer.style.display = "flex";
		productImageContainer.style.flexDirection = "column";

		if (document) {
			var btn = document.getElementById("svg2img-button");
			if (btn)
				btn.addEventListener(
					"click",
					/*#__PURE__*/ _asyncToGenerator(
						/*#__PURE__*/ regeneratorRuntime.mark(function _callee() {
							return regeneratorRuntime.wrap(function _callee$(_context) {
								while (1) {
									switch ((_context.prev = _context.next)) {
										case 0:
											if (!selectedVariant) {
												_context.next = 3;
												break;
											}

											_context.next = 3;
											return svg2img("test.png");

										case 3:
										case "end":
											return _context.stop();
									}
								}
							}, _callee);
						})
					)
				);
		}
	}
}

function loadBackgroundSvgSource() {
	return monoGramData.backgroundImageSrc;
}

function loadVariantSvgSource() {
	var _monoGramData$variant, _monoGramData$variant2;

	var designElement = window.monoGramData?.designElementSrc || null;

	var svgSrc0 =
		monoGramData &&
		(monoGramData === null || monoGramData === void 0 ? void 0 : monoGramData.variants) &&
		(monoGramData === null || monoGramData === void 0 ? void 0 : monoGramData.variants.length) &&
		monoGramData.variants[0].images &&
		monoGramData.variants[0].images.length &&
		((_monoGramData$variant = monoGramData.variants[0].images[0]) === null || _monoGramData$variant === void 0 ? void 0 : _monoGramData$variant.src);
	var svgSrc1 =
		monoGramData &&
		(monoGramData === null || monoGramData === void 0 ? void 0 : monoGramData.variants) &&
		(monoGramData === null || monoGramData === void 0 ? void 0 : monoGramData.variants.length) > 1 &&
		monoGramData.variants[1].images &&
		monoGramData.variants[1].images.length &&
		((_monoGramData$variant2 = monoGramData.variants[1].images[0]) === null || _monoGramData$variant2 === void 0
			? void 0
			: _monoGramData$variant2.src);
	if (!svgSrc1) console.warn("SVG not found in slot 2");
	return designElement || svgSrc1 || svgSrc0 || "";
}

function initializeNamesInputs() {
	var nameSelector = document.querySelector("[name='properties[Names]']");

	if (nameSelector) {
		nameSelector.addEventListener("change", function (e) {
			var _e$target;
			/**
			 * TODO: Get All Higher Than Value, Get All Equal To or Lower Than Value
			 * FE Higher:
			 *  Set Required: false
			 *  Clear Text
			 *  Display None
			 * FE Lower:
			 *  Set Required: true
			 *  Display: block
			 */

			handleNamesChange(e);
		});
		nameSelector.dispatchEvent(new Event("change"));
	}

	function handleNamesChange(e) {
		var _e$target2;

		var custom = getCustomInputs();
		var namesCount =
			+(e === null || e === void 0 ? void 0 : (_e$target2 = e.target) === null || _e$target2 === void 0 ? void 0 : _e$target2.value) || 2;
		var inputs = custom.inputs;
		var divs = custom.divs;
		handleCustomInputs(inputs, namesCount);
		handleCustomDivs(divs, namesCount);
	}

	function handleCustomDivs(divs, namesCount) {
		var higher = Object.keys(divs)
			.filter(function (k) {
				return +k > namesCount;
			})
			.map(function (k) {
				return divs[k];
			});
		var lower = Object.keys(divs)
			.filter(function (k) {
				return +k <= namesCount;
			})
			.map(function (k) {
				return divs[k];
			});
		higher.forEach(function (div) {
			div.style.display = "none";
		});
		lower.forEach(function (div) {
			div.style.display = "block";
		});
	}

	function handleCustomInputs(inputs, namesCount) {
		var higher = Object.keys(inputs)
			.filter(function (k) {
				return +k > namesCount;
			})
			.map(function (k) {
				return inputs[k];
			});
		var lower = Object.keys(inputs)
			.filter(function (k) {
				return +k <= namesCount;
			})
			.map(function (k) {
				return inputs[k];
			});
		higher.forEach(function (inp) {
			inp.required = false;
			inp.textContent = null;
		});
		lower.forEach(function (inp) {
			inp.required = true;
		});
	}

	function getCustomInputs() {
		var inputs = document.querySelectorAll("input[name^='properties[Name']");
		log({
			inputs: inputs,
		});
		var customInputs = [];

		for (var i = 0; i < inputs.length; i++) {
			var inp = inputs[i];
			customInputs.push(inp);
		}

		log({
			inputs: inputs,
			customInputs: customInputs,
		});
		var inputsMap = customInputs.reduce(function (map, inp) {
			return (map[parseName(inp)] = inp), map;
		}, {});
		var divsMap = customInputs.reduce(function (map, inp) {
			return (map[parseName(inp)] = inp.closest(".mono--app--field")), map;
		}, {});
		return {
			inputs: inputsMap,
			divs: divsMap,
		};
	}

	function parseName(inp) {
		var re = new RegExp("[^0-9]", "g");
		return inp.name.replace("properties[", "").replace("]", "").replace(re, "");
	}
}
`;
